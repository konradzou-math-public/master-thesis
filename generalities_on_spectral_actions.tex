\section{Compatibility with the excursion algebra}
Let \(\Lambda\) be a discrete valuation ring over \(\zl\).
For this section, let everything be base changed to \(\Lambda\), that is we write \(\exc(W,\widehat{G})\) for \(\exc(W,\widehat{G})_{\Lambda}\) and \(Z^1(W,\widehat{G})\) for \(Z^1(W,\widehat{G})_{\Lambda}\).
Let \(\mathcal{C}\) be an idempotent-complete small stable \(\Lambda\)-linear \(\infty\)-category.
Assume we have functorially in finite sets \(I\) exact \(\rep{Q^I}{\Lambda}\)-linear functors 
\begin{equation*}
    \rep{(\widehat{G}\rtimes Q)^I}{\Lambda}\to\End_{\Lambda}(\mathcal{C})^{BW^I}
\end{equation*}
where \(W\) is a discretization of \(W_E/P\) for \(P\) an open subgroup of the wild inertia.
% \begin{lem}\label{lem: spectral action on subcategory}
%     Let \(\mathcal{D}\) be an idempotent-complete small stable \(\Lambda\)-linear subcategory of \(\mathcal{C}\) closed under colimits.
%     Assume that the functors 
%     \begin{equation*}
%         \rep{(\widehat{G}\rtimes Q)^I}{\Lambda}\to\End_{\Lambda}(\mathcal{C})^{BW^I}
%     \end{equation*}
%     restrict to 
%     \begin{equation*}
%         \rep{(\widehat{G}\rtimes Q)^I}{\Lambda}\to\End_{\Lambda}(\mathcal{D})^{BW^I}
%     \end{equation*}
%     Then the induced action of \(\perf([Z^1(W,\widehat{G})/\widehat{G}])\) on \(\mathcal{C}\) preserves \(\mathcal{D}\).
% \end{lem}
% \begin{proof}
%     This is immediate from the construction of the spectral action.
% \end{proof}
\begin{thm}\label{lem: excursion algebra action compatible with spectral action}
    We obtain a map \(\exc(W,\widehat{G})\to\pi_0\bcenter(\mathcal{C})\) from the excursion algebra for \(\widehat{G}\) to the Bernstein center of \(\mathcal{C}\).
    The spectral action gives us a map \(\oo(Z^1(W,\widehat{G}))^{\widehat{G}}\to\pi_0\bcenter(\mathcal{C})\).
    The following diagram commutes 
    \begin{equation*}
        \begin{tikzcd}
            {\exc(W,\widehat{G})} \arrow[d] \arrow[r]         & \pi_0\bcenter(\mathcal{C}) \\
            {\oo(Z^1(W,\widehat{G}))^{\widehat{G}}} \arrow[ru] &                           
            \end{tikzcd}
    \end{equation*}
\end{thm}
\begin{proof}
    By abuse of notation we will consider \(\oo_{F_n}\defined\oo_{[Z^1(F_n,\widehat{G}/\widehat{G}]}\) and \(\oo_{W}\defined\oo_{[Z^1(W,\widehat{G})/\widehat{G}]}\) as objects of \(\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q)))\) and \(\perf(\maps_{BQ}^\Sigma(BW,B(\widehat{G}\rtimes Q)))\) respectively.
    This is possible by \cite[Proposition X.3.3.]{geometrization} and (the consequence of) \cite[Proposition X.3.4.]{geometrization}.
    It suffices to check that 
    \begin{equation}\label{eq: diagram that ought to commute spectral action excursion algebra}
        \begin{tikzcd}
            {\oo(Z^1(F_n,\widehat{G}))^{\widehat{G}}} \arrow[d] \arrow[r]         & \pi_0\bcenter(\mathcal{C}) \\
            {\oo(Z^1(W,\widehat{G}))^{\widehat{G}}} \arrow[ru] &                           
            \end{tikzcd}
    \end{equation}
    commutes for every \(F_n\to W\), where the map \(\oo(Z^1(F_n,\widehat{G}))^{\widehat{G}}\to\pi_0\bcenter(\mathcal{C})\) is constructed as in the proof of \cite[Theorem VIII.4.1.]{geometrization}.
    % Note that 
    % \begin{equation*}
    %     \pi_0\colim_{(n,F_n\to W)}\End_{\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q)))}(\oo_{F_n})=\colim_{(n,F_n\to W)}\oo(Z^1(F_n,B(\widehat{G}\rtimes Q)))^{\widehat{G}}
    % \end{equation*}
    % since \(\pi_0\) commutes with colimits.
    % Thus an element \(f\in\exc(W,B(\widehat{G}\rtimes Q))\) gives rise to an object in \(\colim_{(n,F_n\to W)}\arr(\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q))))\).
    % Under the natural map 
    % \begin{align*}
    %     \colim_{(n,F_n\to W)}\arr(\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q))))\to&\arr(\colim_{(n,F_n\to W)}\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q))))\\
    %     \simeq&\arr(\perf(\maps_{BQ}^\Sigma(BW,B(\widehat{G}\rtimes Q))))
    % \end{align*}
    % this is the map \(\exc(W,\widehat{G})\to\oo(Z^1(W,\widehat{G}))^{\widehat{G}}\).
    % This is clear if we drop the ``\(\Sigma\)'' (that is to consider perfect complexes on the genuine mapping stacks), but the categories \(\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q)))\) and \(\perf(\maps_{BQ}^\Sigma(BW,B(\widehat{G}\rtimes Q)))\) are naturally full subcategories of \(\perf(\maps_{BQ}(BF_n,B(\widehat{G}\rtimes Q)))\) and \(\perf(\maps_{BQ}(BW,B(\widehat{G}\rtimes Q)))\).

    The action of \(\perf(\maps_{BQ}^\Sigma(BW,B(\widehat{G}\rtimes Q)))\simeq \perf(\maps_{BQ}(BW,B(\widehat{G}\rtimes Q)))\) is constructed by taking a sifted colimit of actions by \(\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q)))\) and sifted colimits of monoidal stable \(\infty\)-categories are computed underlying.
    Thus via the functor \(\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q)))\to\perf(\maps_{BQ}^\Sigma(BW,B(\widehat{G}\rtimes Q)))\) the spectral actions gives us a commutative diagram 
    \begin{equation*}
        \begin{tikzcd}
            {\oo(Z^1(F_n,\widehat{G}))^{\widehat{G}}} \arrow[d] \arrow[r] & \pi_0\bcenter(\mathcal{C}) \\
            {\oo(Z^1(W,\widehat{G}))^{\widehat{G}}} \arrow[ru]            &                           
            \end{tikzcd}
    \end{equation*}
    via the actions of \(\oo(Z^1(F_n,\widehat{G}))^{\widehat{G}}\) on \(\oo_{F_n}*-\) and \(\oo(Z^1(W,\widehat{G}))^{\widehat{G}}\) on \(\oo_{W}*-\).
    The diagram fits into \eqref{eq: diagram that ought to commute spectral action excursion algebra} above by being part of the diagram 
    \begin{equation*}
        \begin{tikzcd}
            {\oo(Z^1(F_n,\widehat{G}))^{\widehat{G}}} \arrow[r, "\eta"] \arrow[d,"\id"]    & \pi_0\bcenter(\mathcal{C}) \\
            {\oo(Z^1(F_n,\widehat{G}))^{\widehat{G}}} \arrow[d] \arrow[ru, "\alpha"] &                            \\
            {\oo(Z^1(W,\widehat{G}))^{\widehat{G}}} \arrow[ruu]                      &                           
            \end{tikzcd}
    \end{equation*}
    The outer triangle is the diagram \eqref{eq: diagram that ought to commute spectral action excursion algebra}.
    Thus to show that the outer triangle commutes it suffices to check that the upper triangle commutes, where \(\eta\) comes from excursion data and \(\alpha\) comes from the spectral action.
    % Meanwhile the action of the excursion algebra is constructed by taking a sifted colimit of ring maps \(\exc(F_n,\widehat{G})\cong\oo(Z^1(F_n,\widehat{G}))^{\widehat{G}}\to\pi_0\bcenter(\mathcal{C})\) built from excursion data.
    Thus to prove the theorem we may replace \(W\) by a free group \(F_n\) on \(n\) generators with a map \(F_n\to Q\).
    Recall that given an excursion datum \((I,V,\alpha,\beta,(\gamma_i)_{i\in I})\) with \(I\) a finite set, \(V\in\rep{\widehat{G}\rtimes Q}{\Lambda}\), \(\alpha\from 1\to V|_{\rep{\widehat{G}}{\zl}}\), \(\beta\from V|_{\rep{\widehat{G}}{\zl}}\to 1\) and \(\gamma_i\in F_n\) we  get an element in \(\pi_0\bcenter(\mathcal{C})\) by considering 
    \begin{equation*}
        \id=T_1\xto{T_{\alpha}}T_V\xto{(\gamma_i)_{i\in I}}T_V\xto{T_\beta} T_1=\id
    \end{equation*}
    In general we have the following natural commuting triangle 
    \begin{equation*}
        \begin{tikzcd}
            {\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q)))} \arrow[r, hook] & {\perf(\maps_{BQ}(BF_n,B(\widehat{G}\rtimes Q)))}                                                                \\
                                                                                     & \rep{\widehat{G}\rtimes Q}{\Lambda}\subset\perf(B(\widehat{G}\rtimes Q)) \arrow[lu, "\ev_{\Sigma}^*"] \arrow[u, "\ev^*"']
            \end{tikzcd}
    \end{equation*}
    and \(\ev^*\) factors over \(\rep{\widehat{G}\rtimes Q}{\Lambda}\to\rep{\widehat{G}}{\Lambda}\) induced by \(\widehat{G}\to\widehat{G}\rtimes Q\).
    In particular \(\ev^*V\in\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q)))\) for any \(V\in\rep{\widehat{G}\rtimes Q}{\Lambda}\) and \(\ev^*V=\ev^*_{\Sigma}V\).
    This means given an excursion datum as above we may construct the element in \(\pi_0\bcenter(\mathcal{C})\) by considering 
    \begin{equation*}
        \id=\oo_{F_n}*-=\ev^*1*-\xto{\ev^*\alpha}\ev^*V*-\xto{(\gamma_i)_{i\in I}}\ev^*V*-\xto{\ev^*\beta}\ev^*1*-=\oo_{F_n}*-=\id
    \end{equation*}
    From this we see that the induced element in the center \(\pi_0\bcenter(\mathcal{C})\) comes from an endomorphism of \(\oo_{F_n}\).
    Thus we may assume that \(\mathcal{C}\) is \(\perf(\maps_{BQ}^\Sigma(BF_n,B(\widehat{G}\rtimes Q))\).
    Unwinding the construction of the map \(\exc(F_n,\widehat{G})\to\pi_0\bcenter(\mathcal{C})\) we must show the following:

    Given 
    \begin{equation*}
        f\in\oo(\widehat{G}\backslash (\widehat{G}\rtimes Q)^{\{0,\dots,n\}}/\widehat{G})\subset\oo((\widehat{G}\rtimes Q))^{\otimes\{0,\dots,n\}}
    \end{equation*}
    that can be written as \(f=\sum_{i\in I}f_0^{(i)}\otimes\dots\otimes f_n^{(i)}\),
    we construct \(V_f\) as the \((\widehat{G}\rtimes Q)^{\{0,\dots,n\}}\)-subrepresentation of \(\oo((\widehat{G}\rtimes Q)^{\{0,\dots,n\}}/\widehat{G})\) spanned by \(f\).
    It is equipped with maps \(\alpha_f\from 1\to V_f\) induced by \(f\) and \(\beta_f\from V_f\to 1\) induced by evaluation at \(1\in(\widehat{G}\rtimes Q)^{\{0,\dots,n\}}\).
    The map \(\oo_{F_n}\to\oo_{F_n}\) induced by the excursion datum \((V_f,\alpha_f,\beta_f,(1,\gamma_1,\dots,\gamma_n))\) where \(\gamma_i\) are the generators of \(F_n\) is given by 
    \begin{equation*}
        \sum_{i\in I}\varepsilon(f_0^{(i)})\otimes\overline{f_1^{(i)}}\otimes\dots\otimes\overline{f_n^{(i)}}
    \end{equation*}
    where \(\overline{\varepsilon}\from\oo(\widehat{G}\rtimes Q)\to\Lambda\) is the counit and \(f_i\mapsto\overline{f_i}\) is the natural map \(\oo(\widehat{G}\rtimes Q)\to\oo(\widehat{G})\). 
    Since everything in the following will be linear, we assume that \(|I|=1\) for notational simplicity and drop the superscripts.

    Observe that \(V_f\subset\oo((\widehat{G}\rtimes Q)^{\{0,\dots,n\}}/\widehat{G})\subset\oo((\widehat{G}\rtimes Q)^{\{0,\dots,n\}})\) as \((\widehat{G}\rtimes Q)^{\{0,\dots,n\}}\)-representations.
    This means the \(\oo((\widehat{G}\rtimes Q)^{\{0,\dots,n\}})\)-comodule structure is easily described.
    Namely let \(g=g_0\otimes\dots\otimes g_n\in V_f\). The coaction \(\rho\) agrees with the comultiplication \(\Delta\) for \(\oo((\widehat{G}\rtimes Q)^{\{0,\dots,n\}})\).
    If \(\overline{\Delta}\) denotes the comultiplication on \(\oo(\widehat{G}\rtimes Q)\), then 
    \begin{equation*}
        \rho(g)=\Delta(g)=\overline{\Delta}(g_0)\otimes\dots\otimes\overline{\Delta}(g_n)
    \end{equation*}
    Let \(h\in\oo(\widehat{G}\rtimes Q)\).
    If we write \(\oo(Z^1(F_n,\widehat{G}))=\oo(\widehat{G})^{\otimes n}\), then \(\gamma_i^*h=1\otimes\dots\otimes \overline{h}\otimes\dots\otimes 1\) where \(\overline{h}\) is in the \(i\)-th position and \(1^*h=\varepsilon(h)\).
    Here we use the notation of \cref{lem: spectral action spectral side general nonsense}.
    Thus the action of \((1,\gamma_1,\dots,\gamma_n)\) on \(f\in\ev^*V_f=V_f\otimes\oo(Z^1(F_n,\widehat{G}))\) is given by 
    \begin{equation*}
        (1,\gamma_1,\dots,\gamma_n).f=(\id\otimes 1^*)(\overline{\Delta}(f_0))\otimes (\id\otimes \gamma_1^*)(\overline{\Delta}(f_1))\otimes\dots\otimes (\id\otimes \gamma_n^*)(\overline{\Delta}(f_n))
    \end{equation*}
    finally, applying \(\ev^*V_f\to\ev^*1\) is just given by applying \(\varepsilon\otimes \id\).
    If we let \(\overline{\varepsilon}\) denote the counit of \(\oo((\widehat{G}\rtimes Q)^{\{0,\dots,n\}})\), then \(\varepsilon=\overline{\varepsilon}^{\otimes n}\)
    Note that the following diagram commutes by definition 
    \begin{equation*}
        \begin{tikzcd}
            \oo(\widehat{G}\rtimes Q) \arrow[r, "\overline{\Delta}"] \arrow[rd, "\id"'] & \oo(\widehat{G}\rtimes Q)\otimes \oo(\widehat{G}\rtimes Q) \arrow[d, "\overline{\varepsilon}\otimes\id"] \arrow[r] & \oo(\widehat{G}\rtimes Q)\otimes\oo(\widehat{G}) \arrow[ldd, "\overline{\varepsilon}\otimes\id"] \\
                                                                                        & \oo(\widehat{G}\rtimes Q) \arrow[d]                                                                                &                                                                                                  \\
                                                                                        & \oo(\widehat{G})                                                                                                    &                                                                                                 
            \end{tikzcd}
    \end{equation*}
    and \((\overline{\varepsilon}\otimes\overline{\varepsilon})\circ\overline{\Delta}\cong\overline{\varepsilon}\).
    Thus we have 
    \begin{equation*}
        (\varepsilon\otimes\id)((\id\otimes 1^*)(\overline{\Delta}(f_0))\otimes (\id\otimes \gamma_1^*)(\overline{\Delta}(f_1))\otimes\dots\otimes (\id\otimes \gamma_n^*)(\overline{\Delta}(f_n)))=\varepsilon(f_0)\otimes\overline{f_1}\otimes\dots\otimes\overline{f_n}
    \end{equation*}
    like we wanted.
\end{proof}
\begin{rem}
    Everything in this section also works whenever the spectral action is constructed as in \cite[Section X.3]{geometrization} (note that the construction \cite[Section X.1]{geometrization} can be considered as a special case).
    In particular we will see that it works for tori without any restrictions on \(\ell\).
    Additionally, instead of considering \(P\) an open subgroup of the wild inertia we can also consider any open subgroup of \(W_E\), as long as the construction of the spectral action in \cite[Section X.3]{geometrization} still goes through.
    We will see that it works for \(P=\ker(W_E\to W_{F/E}/P')\) where \(P'\subset F^\times\) is open.
\end{rem}