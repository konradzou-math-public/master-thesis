\section{Proof for tori}
We almost have all the ingredients to prove the categorical form of Fargues's conjecture for tori.
One missing piece is removing the restrictions on \(\ell\) in \cite[Theorem VIII.5.1.]{geometrization}.
Let \(W\) be the discretization of \(W_E/P\) that surjects to \(Q\) where \(P\) is any open subgroup of \(W_E\) contained in \(\ker(W_E\to Q)\).
\begin{lem}
    For any prime \(\ell\) the map 
    \[
        \colim_{(n,F_n\to W)} \oo(Z^1(F_n,\widehat{T}))\to \oo(Z^1(W,\widehat{T}))
    \]
    is an isomorphism in the presentable stable $\infty$-category $\mathrm{Ind} \perf(B\widehat{T})$. Moreover, the $\infty$-category $\perf(Z^1(W,\widehat{T})/\widehat{T})$ is generated under cones and retracts by $\perf(B\widehat{T})$, and $\mathrm{Ind}\perf(Z^1(W,\widehat{T})/\widehat{T})$ identifies with the $\infty$-category of modules over $\oo(Z^1(W,\widehat{T}))$ in $\mathrm{Ind}\perf(B\widehat{T})$.
\end{lem}
\begin{proof}
    Recall that this reduces to checking the conditions in \cite[Theorem VIII.5.8.]{geometrization} after base changing to \(\overline{\bbF_{\ell}}\).
    Note that all closed \(\widehat{T}\)-orbits of \(Z^1(W_E,\widehat{T})\) are isomorphic to \(\widehat{T}/\widehat{T}^Q\).
    Since the representation theory of \(\widehat{T}\) is semisimple even over \(\overline{\bbF_{\ell}}\), all conditions are clear, except for the image of \(\perf(B\widehat{T})\to\perf(B\widehat{T}^Q)\) generating under cones and retracts.
    We show that this functor is even essentially surjective.
    Using \cref{prop: spectral category is automorphic category}, \cref{cor: product of derived categories is derived category of products} and \cite[Theorem C.1.]{hnr19} (\(B\widehat{T}\) and \(B\widehat{T}^Q\) are quasi-compact stacks with affine diagonal), we have the identifications 
    \begin{alignat*}{8}
        \perf(B\widehat{T})\subset\D^+_{\qcoh}(B\widehat{T})&\simeq\D^+(\qcoh(B\widehat{T}))\subset\prod_{\chi\in X^*(\widehat{T})}\D(\overline{\bbF_\ell})\\
        \perf(B\widehat{T}^Q)\subset\D^+_{\qcoh}(B\widehat{T}^Q)&\simeq\D^+(\qcoh(B\widehat{T}^Q))\subset\prod_{\chi\in X^*(\widehat{T}^Q)}\D(\overline{\bbF_\ell})
    \end{alignat*}
    A family \((A_\chi)_{\chi\in X^*(\widehat{T})}\) with \(A_\chi\in\D(\overline{\bbF_\ell})\) corresponds to an element in \(\perf(B\widehat{T})\) if and only if \(\bigoplus_{\chi\in X^*(\widehat{T})}A_\chi\in\D(\overline{\bbF_{\ell}})\) is perfect, similarly if we replace \(\widehat{T}\) by \(\widehat{T}^Q\).
    The functor \(\perf(B\widehat{T})\to\perf(B\widehat{T}^Q)\) is given by 
    \begin{equation*}
        (A_\chi)_{\chi\in X^*(\widehat{T})}\mapsto \left(\bigoplus_{\chi\in[\psi]}A_\chi\right)_{[\psi]\in X^*(\widehat{T}^Q)=X^*(\widehat{T})_Q}
    \end{equation*}
    From this description it is clear that it is essentially surjective.
\end{proof}
Thus the construction of the spectral action in \cite[Section X.3]{geometrization} works for all primes \(\ell\) in the case of tori and we have that \(\exc(W,\widehat{T})\to\oo(Z^1(W,\widehat{T}))^{\widehat{T}}\) is an isomorphism.
\begin{thm}\label{thm: categorical form Fargues' conjecture tori}
	The categorical form of Fargues's conjecture holds in the following form:
	For any prime \(\ell\) there is an equivalence of stable $\infty$-categories
	\[
		\D^{b,\mathrm{qc}}_{\coh,\nilp}([Z^1(W_E,\widehat{T})/\widehat{T}])\simeq \D_{\lis}(\bun{T},\zl)^\omega
	\]
	equipped with their actions of $\perf([Z^1(W_E,\widehat{T})/\widehat{T}])$. After taking ind-categories or restricting to connected components the structure sheaf gets mapped to the (unique choice of) Whittaker sheaf \(\mathcal{W}\).
\end{thm}
\begin{proof}
    Letting \(\perf([Z^1(W_E,\widehat{T})/\widehat{T}])\) act on \(\mathcal{W}\) gives a functor 
    \begin{equation*}
        -*\mathcal{W}\from \perf([Z^1(W_E,\widehat{T})/\widehat{T}])\to\D_{\lis}(\bun{T},\zl)
    \end{equation*}
    Recall that \(\D^{b,\mathrm{qc}}_{\coh,\nilp}([Z^1(W_E,\widehat{T})/\widehat{T}])=\perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])\), as the nilpotent cone in \(\widehat{\mathfrak{t}}^*\) is \(\{0\}\) and \cite[Theorem VIII.2.9]{geometrization}.
    We claim that the restriction 
    \begin{equation*}
        -*\mathcal{W}\from \perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])\to\D_{\lis}(\bun{T},\zl)
    \end{equation*}
    is fully faithful with essential image the compact objects in \(\D_\lis(\bun{T},\zl)\).
    Then the compatibility assumption holds automatically.
    Thus to prove the theorem we need to check this claim.
    \cref{lem: reduction to degree 0 parts} tells us that we only need to check that the restriction 
    \begin{equation*}
        -*\mathcal{W}\from \perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])\to\D(T(E)\mhyphen\Mod_{\zl})^{\omega}
    \end{equation*}
    is an equivalence.
    We know that
    \begin{equation*}
        (\perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])=\bigcup_{P\subset F^\times}\perf^{\qc}([Z^1(W_{F/E}/P,\widehat{T})/\widehat{T}])
    \end{equation*}
    where \(P\) runs over open subgroups of \(F^\times\) such that \(\Theta(P)\) is pro-\(p\).
    By \cref{lem: condensed H_1 of quotient is torus quotient} we may additionally assume that the \(\Theta(P)\) are pro-\(p\).
    Then using \cref{lem: compact T(E)-representations as filtered union of perfect modules} and \cref{cor: reduction of categorical Fargues's conjecture to commutative algebra} we only need to check that the restriction 
    \begin{equation*}
        \perf([Z^1(W_E/P,\widehat{T})/\widehat{T}])_0\to\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl})^\omega
    \end{equation*}
    is an equivalence, for \(P\) as above.
    Having the spectral action we get a ring map \(\oo(Z^1(W_E,\widehat{T}))^{\widehat{T}}\to\pi_0\bcenter(\D(T(E)\mhyphen\Mod_{\zl}))\) induced by the action of \(\oo(Z^1(W_E,\widehat{T})\) on \(\oo*-\) which under the identification
    \begin{align*}
        \oo(Z^1(W_E,\widehat{T}))^{\widehat{T}}=\oo(\Hom(T(E),\gm))&=\varprojlim_{P\subset F^\times}\zl[T(E)/\Theta(P)]\\
        \shortintertext{and}
        \pi_0\bcenter(\D(T(E)\mhyphen\Mod_{\zl}))&=\varprojlim_{P\subset F^\times}\zl[T(E)/\Theta(P)]
    \end{align*}
    is an isomorphism.
    This follows from \cite[Proposition IX.6.5.]{geometrization} and \cref{lem: excursion algebra action compatible with spectral action} (note that while \cite[Proposition IX.6.5.]{geometrization} talks about the spectral Bernstein center, it actually computes the action of the excursion algebra).
    Under this isomorphism both \(\perf([Z^1(W_E/P,\widehat{T})/\widehat{T}])\) and \(\prod_{b\in B(T)}\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl})\) are given by those objects \(A\) in \(\perf([Z^1(W_E/P,\widehat{T})/\widehat{T}])\) or \(\D(T(E)\mhyphen\Mod_{\zl})\) such that \(e_P\) acts by the identity for some idempotent \(e_P\in\varprojlim_{P\subset F^\times}\zl[T(E)/\Theta(P)]\).
    The action of \(\oo(Z^1(W_E/P,\widehat{T})^{\widehat{T}}\) on \(\oo*-\) induces a ring map \(\oo(Z^1(W_E/P,\widehat{T}))^{\widehat{T}}\to\pi_0\bcenter(\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl}))\), which fits into the following commutative diagram by \cref{lem: idempotents of the bernstein center}:
    \begin{equation*}
        \begin{tikzcd}
            {\oo(Z^1(W_E,\widehat{T})^{\widehat{T}}} \arrow[r] \arrow[d, "e_P"'] & \pi_0\bcenter(\D(T(E)/\mhyphen\Mod_{\zl})) \arrow[d, "e_P"] \\
            {\oo(Z^1(W_E/P,\widehat{T})^{\widehat{T}}} \arrow[r]                       & \pi_0\bcenter(\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl}))             
            \end{tikzcd}
    \end{equation*}
    As observed before the top horizontal arrow is an isomorphism and as \(e_P\) is an idempotent it follows that the bottom horizontal arrow is an isomorphism too.
    Note that \(\perf([Z^1(W_E/P,\widehat{T})/\widehat{T}])_0=\perf(\zl[T(E)/\Theta(P)])=\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl})^{\omega}\) and the functor 
    \begin{equation*}
        -*\mathcal{W}\from\perf([Z^1(W_E/P,\widehat{T})/\widehat{T}])_0\to\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl})^\omega
    \end{equation*}
    is given by letting \(\perf([Z^1(W_E/P,\widehat{T})/\widehat{T}])_0\) act on \(\zl[T(E)/\Theta(P)]\).
    The following diagram commutes:
    \begin{equation*}
        \begin{tikzcd}
            {\End_{\D(\zl[T(E)/\Theta(P))]}(\zl[T(E)/\Theta(P)])} \arrow[r, "-*\mathcal{W}"] & {\End_{\D(\zl[T(E)/\Theta(P)])}(\zl[T(E)/\Theta(P)])} \arrow[d, "a"] \\
            {\oo(Z^1(W_E/P,\widehat{T})^{\widehat{T}}} \arrow[r] \arrow[u, "r"]          & \pi_0\bcenter(\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl}))                 
            \end{tikzcd}
    \end{equation*}
    Here \(r\) is the map that sends \(s\in\oo(Z^1(W_E/P,\widehat{T}))^{\widehat{T}}\) to right multiplication with \(s\) and \(a\) is induced by the action of \(\End_{\zl[T(E)/\Theta(P)]}(\zl[T(E)/\Theta(P)])\) on 
    \begin{equation*}
        \zl[T(E)/\Theta(P)]\otimes_{\zl[T(E)/\Theta(P)]} -=\id_{\D(T(E)/\Theta(P)\mhyphen\Mod_{\zl})}
    \end{equation*}
    As observed above the bottom arrow is an isomorphism, so is \(r\) and \(a\), hence \(-*\mathcal{W}\) induces an isomorphism between the endomorphism rings of the tensor units.
    We can now apply \cref{lem: equivalence on endomorphism ring of tensor unit implies equivalence on perfect complexes} to finish the proof.
\end{proof}
Taking ind-categories we immediately obtain the statement from the introduction:
\begin{thm}\label{thm: categorical form Fargues' conjecture tori introduction}
	The categorical form of Fargues's conjecture holds in the following form:
	For any prime \(\ell\) there is an equivalence of stable $\infty$-categories
	\[
		\D_{\qcoh}([Z^1(W_E,\widehat{T})/\widehat{T}])\simeq \D_{\lis}(\bun{T},\zl)
	\]
	equipped with their actions of $\perf([Z^1(W_E,\widehat{T})/\widehat{T}])$. 
    The structure sheaf gets mapped to the (unique choice of) Whittaker sheaf \(\mathcal{W}\).
\end{thm}
\begin{proof}
    The only contentious part might be 
    \begin{equation*}
        \mathrm{Ind}(\D^{b,\mathrm{qc}}_{\coh,\nilp}([Z^1(W_E,\widehat{T})/\widehat{T}]))\simeq\D_{\qcoh}([Z^1(W_E,\widehat{T})/\widehat{T}])
    \end{equation*}
    We have \(\D^{b,\mathrm{qc}}_{\coh,\nilp}([Z^1(W_E,\widehat{T})/\widehat{T}]=\perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])\) as observed in the previous proof.
    Decomposing \(Z^1(W_E,\widehat{T})=\bigsqcup_{i\in I}X_i\) into a disjoint union of affine schemes we get
    \begin{equation*}
        \D_{\qcoh}([Z^1(W_E,\widehat{T})/\widehat{T}])=\prod_{i\in I}\D_{\qcoh}([X_i/\widehat{T}])
    \end{equation*}
    Under this decomposition perfect complexes with quasi-compact support correspond to \(\bigoplus_{i\in I}\perf([X_i/\widehat{T}])\).
    By \cite[Example 8.6]{hr17} \(\D_{\qcoh}([X_i/\widehat{T}])\) is compactly generated and \([X_i/\widehat{T}]\) is quasi-compact with affine diagonal.
    By \cite[Theorem 1.2]{hr17} we have \(\D_{\qcoh}([X_i/\widehat{T}])\simeq\D(\qcoh([X_i/\widehat{T}]))\).
    Since \([X_i/\widehat{T}]\) is a quasi-compact open and closed substack of \([Z^1(W_E,\widehat{T})/\widehat{T}]\) it is a gerbe over some quasi-compact open and closed subscheme of \(Y_i\subset\Hom(T(E),\gm)\) banded by \(B\widehat{T}^Q\).
    Using the same proof as for \cref{lem: qcoh decomposition} and using \cref{cor: product of derived categories is derived category of products} we have 
    \begin{equation*}
        \D_{\qcoh}([X_i/\widehat{T}])\simeq\D(\qcoh([X_i/\widehat{T}]))\simeq\prod_{[\chi]\in X^*(\widehat{T})_Q}\D(\qcoh(Y_i))
    \end{equation*}
    Under these equivalences the structure sheaf corresponds to the structure sheaf of \(Y_i\) concentrated in the factor corresponding to \(0\in X^*(\widehat{T})_Q\).
    Since \(Y_i\) is qcqs, we have \(\D(\qcoh(Y_i))\simeq\D_{\qcoh}(Y_i)\) (\cite[Tag 09IS]{stacks} and \cite[Theorem 1.2]{hnr19}) and the perfect complexes are compact (\cite[Tag 09M1]{stacks}).
    It follows that the \(\oo_{[X_i/\widehat{T}]}\) is a compact object and by \cite[Remark 4.6]{hr17} all perfect complexes on \([X_i/\widehat{T}]\) are compact.
    We conclude by \cref{lem: ind categories direct sum stable infinity categories}.
\end{proof}