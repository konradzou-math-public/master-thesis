\addchap{Introduction}
The local Langlands correspondence seeks to find a map from the set of irreducible smooth representations of \(G(E)\) to the set of \(L\)-parameters for \(G\), for a reductive group \(G\) over a local field \(E\).
This map should satisfy a list of properties, although even the correct formulation is conjectural.
In \cite[]{geometrization} Fargues and Scholze propose a reformulation of the local Langlands correspondence in the style of the geometric Langlands program.
In particular they conjecture that the category \(\D_\lis(\bun{G})\) of lisse sheaves on the stack of \(G\)-bundles \(\bun{G}\) is equivalent to the category of certain coherent sheaves on \([Z^1(W_E,\widehat{G})/\widehat{G}]\), the stack of \(L\)-parameters for \(G\).
Additionally they construct the spectral action: an action of \(\perf([Z^1(W_E,\widehat{G})/\widehat{G}])\) on \(\D_\lis(\bun{G})\) and the equivalence should be compatible with this action.
This equivalence should depend on the choice of a Whittaker datum.
Such a datum determines a lisse sheaf on \(\bun{G}\) and the structure sheaf of \([Z^1(W_E,\widehat{G})/\widehat{G}]\) should be mapped to this.
This is a categorical form of Fargues' geometrization conjecture that appeared in \cite[]{farguesgeometrization}.
Naturally the first case to check this conjecture is for the case of commutative reductive groups, i.e. tori.
Despite what the title says we actually prove the conjecture in this generality.
The precise statement we prove is the following:
\begin{thm}[\cref{thm: categorical form Fargues' conjecture tori introduction}]
    For any prime \(\ell\) there is an equivalence of stable $\infty$-categories
	\[
		\D_{\qcoh}([Z^1(W_E,\widehat{T})/\widehat{T}])\simeq \D_{\lis}(\bun{T},\zl)
	\]
	equipped with their actions of $\perf([Z^1(W_E,\widehat{T})/\widehat{T}])$. 
    The structure sheaf gets mapped to the (unique choice of) Whittaker sheaf \(\mathcal{W}\).
\end{thm}

The idea is relatively simple.
Let \(T\) be a torus over \(E\).
Recall that Langlands duality for tori is usually formulated as the statement 
\begin{equation}\label{eq: langlands duality tori}
    H^1_{\mathrm{cont}}(W_E,\widehat{T}(\bbC))\cong\Hom_{\mathrm{cont}}(T(E),\bbC^\times)
\end{equation}
where the left hand side is the continuous group cohomology and the right hand side are the continuous group homomorphisms.
A more careful consideration of Langlands proof of this proposition shows that we can obtain a geometric statement:

\([Z^1(W_E,\widehat{T})/\widehat{T}]\) is a gerbe over \(\Hom(T(E,\gm))\) banded by \(B\widehat{T}^Q\).
Here \(Z^1(W_E,\widehat{T})\) parametrizes condensed or equivalently continuous cocycles \(W_E\to\widehat{T}(\Lambda)\) where \(\Lambda\) is a \(\zl\)-algebra equipped with the relatively discrete topology with respect to \(\zl\).
\(\Hom(T(E),\gm)\) parametrizes the continuous/condensed homomorphisms \(T(E)\to\Lambda^\times\).
The main difficulty in Langlands proof of \cref{eq: langlands duality tori} is to show that we have an isomorphism of abelian groups \(H_1(W_{F/E},X^*(\widehat{T}))\cong T(E)\), where \(F\) is a field extension that splits \(T\).
For our geometric statement we will need to upgrade this isomorphism to an isomorphism of condensed abelian groups.
Since the prerequisite theory of condensed group (co)homology does not exist yet, we develop it in \cref{condensed group (co)homology}.
The upgrade to an isomorphism of condensed abelian groups happens in \cref{some general topology}.
We proceed rather indirectly, showing that \(H_1(W_{F/E},X_*(\widehat{T}))\) comes from a topological space and then use an open mapping theorem to upgrade a continuous bijection to a homeomorphism.
The quasi-coherent sheaves on \([Z^1(W_E,\widehat{T})/\widehat{T}]\) are given by \(X^*(\widehat{T}^{\mathrm{Gal}_E})=B(T)\)-graded quasi-coherent sheaves on \(\Hom(T(E),\gm)\), which are just given by smooth \(T(E)\)-representations.

On the other hand the stack of \(T\)-bundles decomposes into a disjoint union of classifying stacks for \(T(E)\), indexed by the Kottwitz set \(B(T)\).
Thus \(\D_\lis(\bun{T})\) is the \(B(T)\)-indexed selfproduct of \(\D_\lis([*/\cond{T(E)}])\).
We will show that \(\D_\lis([*/\cond{T}])\) is exactly the derived category of smooth representations of \(T(E)\).
This result independently appeared as \cite[Proposition VII.7.1.]{geometrization}.
Our approach is similar, although we more carefully consider the topological aspects in \cref{topology of relatively discrete zl-modules} and the categorical aspects in \cref{sheaves on [*/G]}.
We also directly obtain the \(\infty\)-enhancement of the result.

Having done that, the desired equivalence in the categorical form of Fargues' conjecture is easily obtained, but without the spectral action.

To obtain compatibility with the spectral action, we show that the functor given by acting on the structure sheaf of \([Z^1(W_E,\widehat{T})/\widehat{T}]\) induces the desired equivalence.

Both sides decompose as a \(B(T)\)-indexed product of categories that can be considered as an increasing union of (the subcategory of compact objects in) module-categories.

In \cref{preservation of grading}, we show that the spectral action preserves the decomposition into \(B(T)\)-indexed products.
For this we will need to compute the Hecke operators explicitly, which we do in \cref{hecke operators}.
In \cref{matching of union}, we show that the spectral action preserves the presentation as an increasing union of categories.
With these two reduction steps we only need to check that a functor between compact objects of module categories is an equivalence.
Additionally we show that the functor is given by an exact action acting on the tensor unit.
In \cref{equivalence criterion} we show that such functors are an equivalence when they induce an equivalence on the endomorphism rings of the tensor units.
It is difficult to check this criterion directly.
Instead, the formalism of excursion data allows us to show that the induced map on centers is an equivalence, which is sufficient to show that the morphism induced on endomorphism rings is an equivalence.
One problem it was not shown in \cite[]{geometrization} that the map induced by excursion data and the map induced by the spectral action agrees.
We show that this is the case in \cref{spectral action}.



% Using our approach of matching the categories first and then considering the spectral action makes directly checking compatibility rather hard, as being compatible with the spectral action requires one needs to check infinitely many higher coherences.
% Therefore we restrict ourself to the heart and carefully understanding the Hecke operators there.
% To get a clean description we introduce the concepts of geodes.
% The main point is that the \(W_E\)-action on the Hecke operators is hard to understand, but using geodes we can restrict ourself to considering the action of \(W_{E_\mu}\) for \(E_\mu/E\) a certain field extension, which is much more tractible.
% Then the Hecke action is just given by taking the derived functor the the Hecke action on the heart.
% One needs to do this highly coherently though to get a statement about \(\infty\)-categorical actions.
% This is rather tedious, it appears as \cref{cor: extending spectral action by deriving}.
% Having done all of this, the categorical form of Fargues' conjecture for tori is easily proven.

\section*{Acknowledgments}
Naturally and with great pleasure I would like to thank my advisor Peter Scholze for answering the countless questions I had and for sharing his thoughts on the matter, in particular how to reduce the proof to matching the centers on both sides.
\section*{Notation}
List of notation and things we fix:
\begin{itemize}
    \item \(\ell\) an auxiliary prime 
    \item \(E\) a non-archimedian local field of residue characteristic \(q\)
    \item \(\overline{E}\) an algebraic closure of \(E\) with completion \(C=\widehat{\overline{E}}\)
    \item \(T\) a torus over \(E\), split over \(F\), \(F\) being a subfield of \(\overline{E}\)
    \item \(Q\) the Galois group of the field extension \(F/E\)
    \item \(\widehat{L}=X_*(T)\), \(L=X^*(T)\), equipped with their canonical \(Q\)-action
    \item \(Q_\mu\) the stabilizer for \(\mu\in X_*(T)\)
    \item \(E_\mu\) the fixed field corresponding to \(Q_\mu\)
    \item \(W_{F/E}\) the relative Weil group, fitting in a short exact sequence 
    \begin{equation*}
        0\to F^\times\to W_{F/E}\to Q\to 0
    \end{equation*}
    %classfied by the fundamental class called \(u_{F/E}\). Sometimes we will fix some set theoretic section \(Q\to W_{F/E}\) and write \((a,q)\) for some \(a\in F^\times\) and \(q\in Q\) and will consider this an element of \(W_{F/E}\).
\end{itemize}
% Inside \(\overline{E}\) we find the maximal unramified extension \(E^{\mathrm{unr}}\) of \(E\).
% The Galois group \(\mathrm{Gal}(E^{\mathrm{unr}}/E)\) has a canonical generator \(\mathrm{Frob}_E\).
% We call \(\pi_E\) its image under the Artin reciprocity.
% Similarly for all field extensions \(K\) of \(E\) in \(\overline{E}\) we a canonical Frobenius \(\mathrm{Frob}_K\), and call \(\pi_K\) its image under the Artin reciprocity.
% Note that using these choices \(\nm(\pi_K)=\pi_E^d\) where \(d\) is the degree of the residue field extension.
% These uniformizers \(\pi_K\) give us a product decomposition \(K^\times\cong \pi_{K}^{\bbZ}\times\oo_{K}^\times\) and we will make use of this identification.
For \(\mu\in X_*(T)\) we write \(\overline{\mu}\in X_*(T)/Q\) and \([\mu]\in X_*(T)_Q=B(T)\) for the images under the projection.
We note that we have a \(Q\)-equivariant identification \(\widehat{T}(\Lambda)=\Hom(\widehat{L},\Lambda^\times)\) which we will use quite often.
We will also freely use the notation of \cite[]{geometrization} with one exception:
Given a stable \(\infty\)-category \(\mathcal{C}\), we will write \(\bcenter(\mathcal{C})\) for the endomorphisms of \(\id_{\mathcal{C}}\) as an \(E_1\)-ring spectrum.
Taking \(\pi_0\) gives the usage in \cite[]{geometrization}.