\section{Preservation of \texorpdfstring{$B(T)$}{B(T)}-grading}\label{preservation of grading}
\begin{defn}
    Let \(\mathcal{W}\) denote the lisse sheaf on \(\bun{T}\) associated to the representation \(\cind{*}{T(E)}\) that is attached to the unique Whittaker datum for \(T\).
\end{defn}
\begin{lem}\label{lem: preservation of B(T)-grading}
    The functor
    \begin{equation*}
        -*\mathcal{W}\from\perf([Z^1(W_E,\widehat{T})/\widehat{T}])\to\D_{\lis}(\bun{T},\zl)
    \end{equation*}
    induced by acting on the Whittaker sheaf \(\mathcal{W}\) preserves the decomposition into \(B(T)\)-graded pieces.
\end{lem}
\begin{proof}
    Observe that the decomposition of \(\D_{\lis}(\bun{T},\zl)\) into \(B(T)\) factors according to \(\pi_0(\bun{T})\) and the decomposition according to the connected components of \(Z^1(W_E,\widehat{T})\) commute with each other.
    Thus we can decompose \(\D_{\lis}(\bun{T},\zl)\) into a product indexed by \(B(T)\times\pi_0(Z^1(W_E,\widehat{T}))\).
    Each of the factors will be preserved by colimits and retracts.
    Thus \cref{lem: degree 0 piece thick stable subcategory of structure sheaf} implies that the degree 0 piece matches up, as the structure sheaf gets mapped to the Whittaker sheaf.
    For general \([\mu]\in B(T)\), note that \(\oo[-\mu]\otimes-\) moves the degree \([\mu]\)-piece of \(\perf([Z^1(W_E,\widehat{T})/\widehat{T}])\) to degree 0.
    We claim that \(\oo[-\mu]*-\) moves the degree \([\mu]\)-piece of \(\D_{\lis}(\bun{T},\zl)\) to degree 0.
    For this note that \(\oo[-\mu]\) is a retract of \(g^*V_{-\mu}\) where \(g\from [Z^1(W_E,\widehat{T})/\widehat{T}]\to B(\widehat{T}\rtimes Q)\) is the natural map and \(V_{-\mu}\) is a \(\widehat{T}\rtimes Q\)-representation that is concentrated in the single \(Q\)-orbit \(Q.(-\mu)\).
    Thus \(\oo[-\mu]*-\) is a retract of \(g^*V_{-\mu}*-=T_{V_{-\mu}}\).
    The claim follows from \cref{cor: explicit hecke operators}.
\end{proof}
\begin{cor}
    The functor \(-*\mathcal{W}\from\perf([Z^1(W_E,\widehat{T})/\widehat{T}])\to\D_{\lis}(\bun{T},\zl)\) restricts to a functor 
    \begin{equation*}
        \perf([Z^1(W_E,\widehat{T})/\widehat{T}])_0%
        %=\perf(Z^1(W_E,\widehat{T})/\!\!/\widehat{T})=\perf(\Hom(T(E),\gm))
        \to\D(T(E)\mhyphen\Mod_{\zl})
    \end{equation*}
\end{cor}
\begin{lem}\label{lem: reduction to degree 0 parts}
    Assume that the functor 
    \begin{equation*}
        \perf([Z^1(W_E,\widehat{T})/\widehat{T}])_0\to\D(T(E)\mhyphen\Mod_{\zl})
    \end{equation*}
    restricts to an equivalence 
    \begin{equation*}
        \perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])_0\xto{\simeq}\D(T(E)\mhyphen\Mod_{\zl})^{\omega}
    \end{equation*}
    Then the functor \(-*\mathcal{W}\) restricts to an equivalence
    \begin{equation*}
        \perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])\xto{\simeq}\D_{\lis}(\bun{T},\zl)^{\omega}
    \end{equation*}
\end{lem}
\begin{proof}
    Recall that \(\oo[\chi]\otimes-\) is an autoequivalence of \(\perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])\) and \(\oo[\chi]*-\) is an autoequivalence of \(\D_{\lis}(\bun{T},\zl)^\omega\).
    For fully faithfulness, it suffices to show that for \(A\in\perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}\) and \(B\in\perf^{\qc}([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi']}\) we have \(\rhom(A,B)\cong\rhom(A*\mathcal{W},B*\mathcal{W})\).
    By \cref{lem: preservation of B(T)-grading}, both sides vanish if \([\chi]\neq[\chi']\) and if \([\chi]=[\chi']\), we may even assume \([\chi]=0\) by applying \(\oo[-\chi]\otimes-\).
    Then the desired isomorphism between hom-anima holds by assumption.
    For essential surjectivity, it suffices to check that lisse sheaves \(\mathcal{F}\in\D_{\lis}(\bun{T},\zl)^{\omega}_{[\chi]}\) concentrated in a single component lie in the essential image.
    By (the proof of) \cref{lem: preservation of B(T)-grading}, \(\oo[-\chi]*\mathcal{F}\) likes in the component corresponding to \(0\in B(T)\), so by assumption we have some \(A\in\perf^{qc}([Z^1(W_E,\widehat{T})/\widehat{T}])_0\) such that \(A*\mathcal{W}\cong \oo[-\chi]*\mathcal{F}\).
    Then \(\oo[\chi]\otimes A*\mathcal{W}\cong \mathcal{F}\), so \(\mathcal{F}\) lies in the essential image.
\end{proof}