\section{Condensed proof of Proposition~\ref{prop: transfer condensed iso}}
We record a proof of \cref{prop: transfer condensed iso} using condensed mathematics instead of careful topological considerations that was communicated to us by Peter Scholze.
% \begin{lem}
%     Any filtered category admits a final functor from a directed set.
% \end{lem}
% \begin{proof}
%     This is \cite[Expos\'e I, Proposition 8.1.6.]{SGA4}.
% \end{proof}
% \begin{lem}\label{lem: countable filtered same as N}
%     Any countable filtered category \(I\) admits a final functor from \(\omega\).
% \end{lem}
% \begin{proof}
%     See the proof of \cite[Tag 0597]{stacks}.
% \end{proof}
\begin{defn}
    Let \(\mathcal{C}\) be a category.
    Define \(\mathrm{Seq}(\mathcal{C})\) be the full subcategory of \(\mathrm{Ind}(\mathcal{C})\) spanned by the sequential ind-objects.
\end{defn}
% \begin{rem}
%     By \cref{lem: countable filtered same as N} this is the same as the category spanned by the sequential ind-objects.
% \end{rem}
% \begin{lem}\label{lem: final functor to omega admits good functor from omega}
%     Let \(M\) be a filtered category and \(f_i\from M\to\omega\) a finite collection of final functors.
%     Then there exists a functor \(g\from\omega\to M\) such that \(f_i\circ g\) is a final functor for all \(i\).
% \end{lem}
% \begin{proof}
%     We let \(g(0)\defined m_0\) for any \(m_0\in M\).
%     Assume that \(g\) has been defined up to \([n-1]\subset\omega\).
%     Since the \(f_i\) are final there must be some \(m'_{n,i}\in M\) such that \(f_i(m'_{n,i})\geq f_i(g(n-1))+1\).
%     Since \(M\) is filtered and we only consider finitely many \(f_i\) there is some \(m_n\in M\) with morphisms \(\alpha\from g(n-1)\to m_n\) and \(m'_{n,i}\to m_n\).
%     Then we set \(g(n)\defined m_n\) and \(g(n-1)\to g(n)\) to be the \(\alpha\).
%     The fact that we have a map \(m'_{n,i}\to m_n\) guarantees that \(f_i(g(n))\geq f_i(m'_{n,i})\geq f_i(g(n-1))+1\).
%     This construction forces \(f_i\circ g\) to be strictly increasing, so \(g\) is our desired functor.
% \end{proof}
\begin{lem}\label{lem: representation of morphism sequential ind-object}
    Let \(F,G\from \omega\to \mathcal{C}\) represent two sequential ind-objects in a category \(\mathcal{C}\) and \(f\from ``\colim" F\to ``\colim" G\) a morphism of ind-objects.
    Then there exists a natural transformation \(\eta\from F\to G'\) that induces \(f\), where \(G'=G\circ j\) for some final functor \(j\from\omega\to\omega\).
\end{lem}
\begin{proof}
    This is well-known, but let us include a proof for the reader's convenience.
    The following is taken from \cite[]{lft_notes}.
    Let \(\sigma_i\from F(i)\to F(i+1)\) denote the transition functions in \(F\) and \(\tau_i\from G(i)\to G(i+1)\) the transition functions in \(G\).
    We have 
    \begin{equation*}
        \Hom_{\mathrm{Ind}(\mathcal{C})}(``\colim F",``\colim"G)=\lim_{i\in\omega}\colim_{j\in\omega}\Hom_{\mathcal{C}}(F(i),G(j))
    \end{equation*}
    The morphism \(f\) admits a representation \(([f_0],[f_1],\dots)\) such that \(\sigma_i^*[f_{i+1}]=[f_i]\) for all \(i\) where 
    \begin{align*}
        \sigma_i^*\from\colim_{j}\Hom(F(i+1),G(j))&\to\Hom(F(i),G(j))\\
        [f_{i+1}]&\mapsto [f_{i+1}\circ\sigma_i]
    \end{align*}
    For each \([f_i]\) in the inner colimit we can find a representative \(\eta_i\from X_i\to Y_{j(i)}\) such that \(j(i)\) is minimal.
    This means there is no factorization like 
    \begin{equation*}
        \begin{tikzcd}
            & X_i \arrow[d, "\eta_i"] \arrow[ld, "\eta_i'"'] \\
Y_{j(i)-1} \arrow[r, "\tau_{j(i)-1}"'] & Y_{j(i)}                                      
\end{tikzcd}
    \end{equation*}
    From this we see that minimality guarantees \(j(i)\leq j(i+1)\).
    The condition \(\sigma_i^*[f_{i+1}]=[f_i]\) guarantees that \(\eta_i\) assembles to a natural transformation \(\eta\from F\to G\circ j\).
\end{proof}
\begin{lem}\label{lem: sequential ind-objects closed under finite (co)limits sequential and extensions}
    Let \(\mathcal{B}\) be an abelian category and \(\mathcal{A}\) an exact abelian subcategory of \(\mathcal{B}\) closed under biproducts consisting of compact objects.
    Then \(\mathrm{Seq}(\mathcal{A})\subset \mathcal{B}\) is closed under finite limits, finite colimits, sequential colimits and extensions.
\end{lem}
Note that since \(\mathcal{A}\) consists of compact objects that the natural functor \(\mathrm{Seq}(\mathcal{A})\to \mathcal{B}\) is an embedding, the essential image is spanned by those objects that are sequential colimits of objects in \(\mathcal{A}\).
\begin{proof}
    For biproducts observe that
    \begin{equation*}
        ``\colim_{i\in \omega}"F(i)\oplus ``\colim_{j\in \omega}"G(j)\cong ``\colim_{(i,j)\in \omega\times \omega}"F(i)\oplus F(j)\cong ``\colim_{i\in\omega}"F(i)\oplus G(i)
    \end{equation*}
    Kernels and cokernels are handled by \cref{lem: representation of morphism sequential ind-object}.
    For sequential colimits, note that 
    \begin{equation*}
        \colim_{j\in \omega}``\colim_{i\in \omega}"F_j(i)\cong ``\colim_{(i,j)\in \omega\times\omega}"F_j(i)\cong``\colim_{i\in \omega}"F_i(i)
    \end{equation*}
    % Let 
    % \begin{equation*}
    %     ``\colim_{i\in \omega}"F_0(i)\xto{f}``\colim_{i\in I}"F_1(i)\xto{g}``\colim_{i\in \omega}"F_2(i)\to 0
    % \end{equation*}
    % be a short exact sequence of ind-objects.
    % To show that \(\mathrm{Seq}(\mathcal{A})\) is closed under extensions we need to show that \(``\colim_{i\in I}"F_1(i)\) can be represented by an \(\omega\)-indexed diagram.
    % By (the proof of) \cite[Proposition (3.3)]{etale_homotopy} there is a filtered category \(M\) with final functors \(r_0,r_2\from M\to\omega\) and \(r_1\from M\to I\) such that \(f\) comes from a natural transformation \(\eta\from F_0\circ r_0\to F_1\circ r_1\) and \(g\) comes from a natural transformation \(\varepsilon\from F_1\circ r_1\to F_2\circ r_2\).
    % By \cref{lem: final functor to omega admits good functor from omega} we find a functor \(s\from \omega\to M\) such that \(F_i\circ r_i\circ s\) and \(F_i\) represent the same ind-object for \(i\in\{0,2\}\).
    % Now we have the following commuting diagram between short exact sequences
    % \begin{equation*}
    %     \begin{tikzcd}[column sep = small]
    %         0 \arrow[r] & ``\colim" F_0\circ r_0 \arrow[r, "\eta"]                     & ``\colim" F_1\circ r_1 \arrow[r, "\varepsilon"]                     & ``\colim" F_2\circ r_2 \arrow[r]                  & 0 \\
    %         0 \arrow[r] & ``\colim" F_0\circ r_0\circ s \arrow[u] \arrow[r, "s^*\eta"] & ``\colim" F_1\circ r_1\circ s \arrow[r, "s^*\varepsilon"] \arrow[u] & ``\colim" F_1\circ r_1\circ s \arrow[u] \arrow[r] & 0
    %         \end{tikzcd}      
    % \end{equation*}
    % The two outer arrows are isomorphisms, and thus so is the middle one.
    % The domain of \(F_1\circ r_1\circ s\) is \(\omega\), this finishes the proof.
    % We assume without loss of generality that \(\mathcal{A}\) is skeletal, i.e. that all isomorphisms are equalities.
    % By \cref{lem: representation of morphism general ind-objects} we can find directed sets \(M\) and \(M'\) with final functors \(r_0\from M\to\omega\), \(r_1\from M\to I\), \(r_2\from M'\to I\) and \(r_3\from M'\to\omega\) such that \(f\) is represented by a natural transformation \(\eta\from F_0\circ r_0\to F_1\circ r_1\) and \(g\) is represented by a natural transformation \(\varepsilon\from F_1\circ r_2\to F_2\circ r_3\).
    % We can replace \(F_1\) by its restriction to the image of  \(r_0\) and \(F_3\) by its restriction to the image of \(r_3\)without changing the ind-object, since \(r_0\from M\to\im(r_0)\) is final, similarly for \(r_3\).
    % From now on we write \(F_1\) and \(F_3\) for \(F_1\from N\to \mathcal{A}\) and \(F_3\from N'\to \mathcal{A}\) for some countable totally ordered sets \(N\) and \(N'\) and \(r_0\from M\to N\) and \(r_3\from M'\to N'\) are surjective.
    % Let \(\pr_0,\pr_1\) denote the projections for \(M\times M'\).
    % Note that these are final functors.
    % Then \(f\) and \(g\) are represented by 
    % \begin{align*}
    %     \pr_0^*\eta&\from F_0\circ r_0\circ \pr_0\to F_1\circ r_1\circ\pr_0\\
    %     \shortintertext{and}
    %     \pr_1^*\varepsilon&\from F_1\circ r_2\circ\pr_1\to F_2\circ r_3\circ\pr_3
    % \end{align*}
    % respectively.
\end{proof}
\begin{lem}\label{lem: Z[S] pseudo-coherent}
    Let \(S\) be a compact Hausdorff space.
    Then \(\bbZ[S]\) is a pseudo-coherent condensed abelian group.
\end{lem}
\begin{proof}
    Let \(M=\colim_{i\in I}M_i\) be a filtered colimit of condensed abelian groups.
    Fix some uncountable strong limit cardinal \(\kappa\) such that all \(M_i\), \(M\) and \(\bbZ[S]\) are \(\kappa\)-condensed.
    To check that \(\mathrm{Ext}^n(\bbZ[S],M)=\colim_{i\in I}\mathrm{Ext}^n(\bbZ[S],M_i)\), note that these can be computed in \(\kappa\)-condensed sets.
    The claim follows from the fact that \(\kappa\)-condensed sets from a coherent topos (\(\kappa\)-small compact Hausdorff spaces forming a family of qcqs generators closed under fiber products), so \cite[Expos\'e VI, Corollaire 5.2]{SGA4} applies.
\end{proof}
\begin{lem}\label{lem: compact Hausdorff abelian is pseudo-coherent}
    Compact Hausdorff abelian groups are pseudo-coherent condensed abelian groups.
\end{lem}
\begin{proof}
    Using the Breen-Deligne resolution (see \cite[Theorem 4.10.]{condensed}) and \cref{lem: Z[S] pseudo-coherent}, we see that compact Hausdorff abelian groups admits a resolution by pseudo-coherent condensed abelian groups.
    The claim follows as in \cite[Tag 064Y]{stacks}.
\end{proof}
\begin{lem}\label{lem: compact Hausdorff exact extension closed subcategory}
    Compact Hausdorff abelian groups (or equivalently condensed abelian groups that are qcqs) are an exact abelian subcategory of condensed abelian groups closed under biproducts and extensions.
\end{lem}
\begin{proof}
    By Pontrjagin duality the category of compact abelian Hausdorff groups \(\mathrm{CompAb}\) is equivalent to the opposite category of abelian groups \(\mathrm{Ab}^{\op}\), so it is abelian.
    Preservation of kernels and products by the inclusion is clear, the only thing left to check is cokernels.
    For this it is sufficient to check that if \(f\from A\to B\) is a surjective morphism of compact Hausdorff abelian groups, then \(\cond{f}\from\cond{A}\to\cond{B}\) is surjective.
    This is clear, as surjective morphisms of compact Hausdorff spaces are a covering on the site of compact Hausdorff spaces, which defines condensed sets and \(\cond{f}\) is just a morphism of representable sheaves.
    For extensions, let \(0\to A\to B\to C\to 0\) be a short exact sequence of condensed abelian groups, such that \(A\) and \(C\) are qcqs.
    We want to show that \(B\) is qcqs.
    Let \(\kappa\) be an uncountable strong limit cardinal, such that \(A\), \(B\) and \(C\) are \(\kappa\)-condensed.
    We have a fiber product diagram 
    \begin{equation*}
        \begin{tikzcd}
            A\times B \arrow[r, "+"] \arrow[d, "\pr"'] & B \arrow[d] \\
            B \arrow[r]                                & C          
            \end{tikzcd}
    \end{equation*}
    where one arrow is the projection and the other arrow is the addition map.
    Clearly \(\pr\from A\times B\to B\) is qcqs (being the base change of \(A\to *\) which is qcqs).
    Since \(B\to C\) is surjective, this implies that \(B\to C\) is qcqs (since \(\kappa\)-condensed sets have a family of quasi-compact generators, see \cite[Expos\'e VI, Proposition 1.10]{SGA4}).
    Since \(C\) is qcqs (and \(*\) is qcqs), it follows that \(B\) is also qcqs.
\end{proof}
\begin{lem}\label{lem: sequential colimit Hausdorff closed under extension finite (co)limits}
    The class of condensed abelian groups that are sequential colimits of compact abelian Hausdorff groups is closed under finite limits, finite colimits, sequential colimits and extensions.
\end{lem}
\begin{proof}
    Compact abelian Hausdorff groups give rise to compact condensed abelian groups by \cref{lem: compact Hausdorff abelian is pseudo-coherent}, so we can get everything by \cref{lem: compact Hausdorff exact extension closed subcategory} and \cref{lem: sequential ind-objects closed under finite (co)limits sequential and extensions}, except extensions.
    Let \(0\to F\to G\to H\to 0\) be a short exact sequence of condensed abelian groups, where \(F=\colim_{i\in \omega}F_i\), \(H=\colim_{i\in \omega}H_i\) are sequential colimits of compact Hausdorff abelian groups.
    Then \(G=\colim_{i\in\omega}H_i\times_H G\) by universality of colimits in topoi (or rather one should restrict to \(\kappa\)-small things as in the previous proofs).
    We get short exact sequences \(0\to F\to H_i\times_H G\to H_i\to 0\), so we reduce to the case where \(H\) is compact Hausdorff.
    Then \(H\) is pseudo-coherent, so that \(\mathrm{Ext}^1(H,\colim_{i\in \omega}F_i)=\colim_{i\in\omega}\mathrm{Ext}^1(H,F_i)\).
    Let \(\xi\in \mathrm{Ext}^1(H,\colim_{i\in \omega}F_i)\) represent the extension \(0\to F\to G\to H\to 0\).
    Then the previous equiality of Ext-groups shows that there is some index \(j\in\omega\) and a class \(\xi_j\in \mathrm{Ext}^1(H,F_j)\) that maps to \(\xi\).
    From this we obtain \(\xi_i\in \mathrm{Ext}^1(H,F_i)\) for \(i\geq j\) that are just the image of \(\xi_j\) by the map induced by \(F_i\to F_j\), all mapping to \(\xi\).
    This means we find short exact sequences \(0\to F_i\to G_i\to H\to 0\) functorially in \(i\geq j\) fitting into 
    \begin{equation*}
        \begin{tikzcd}
            0 \arrow[r] & F_i \arrow[d] \arrow[r] & G_i \arrow[d] \arrow[r] & H \arrow[d] \arrow[r] & 0 \\
            0 \arrow[r] & F \arrow[r]             & G \arrow[r]             & H \arrow[r]           & 0
            \end{tikzcd}
    \end{equation*} 
    where the left square is a pushout diagram.
    Let us reindex so that \(j=0\).
    Then \(\colim_{i\in\omega} G_i=\colim_{i\in\omega} F_i\oplus_{\colim_{i\in\omega}F}\colim_{i\in\omega}G=G\) where the colimits over \(F\) and \(G\) are as constant diagrams.
    Note that the map \(\colim_{i\in \omega}F_i\to\colim_{i\in\omega}F\cong F\) identifies with the identity map, which gives the second equality.
    Thus we reduce to the case where \(F\) is also compact Hausdorff, where it is \cref{lem: compact Hausdorff exact extension closed subcategory}.
\end{proof}
\begin{lem}\label{lem: baire application}
    Let \(G\) be a compact Hausdorff group such that \(G=\bigcup_{i=0}^{\infty}G_i\) where \(G_i\) are closed subgroups such that \(G_i\subseteq G_{i+1}\).
    Then \(G=G_N\) for some index \(N\).
\end{lem}
\begin{proof}
    By the Baire category theorem there exists some index \(i_0\) such that \(G_{i_0}\) contains an open subset.
    Thus \(G_{i_0}\) is open and closed.
    Applying the Baire category theorem to \(G\cap G_{i_0}^c=\bigcup G_i\cap G_{i_0}^c\) we find another index \(i_1\) such that \(G_{i_1}\) contains an open subset (using that \(G_{i_0}^c\) is open and closed).
    Continuing inductively we find infinitely many indices \(i_j\) such that \(G_{i_j}\) is open and closed and we have \(G=\bigcup_{j=0}^{\infty}G_{i_j}\).
    Since \(G\) is compact, finitely many \(i_j\) suffice and we can choose \(N\) to be the largest one of those finitely many.
    Then \(G=G_N\).
\end{proof}
\begin{lem}\label{lem: sequential colimit qcqs condensed on underlying set}
    Let \(X\) and \(Y\) be condensed abelian groups that are sequential colimits of compact Hausdorff abelian groups.
    Let \(f\from X\to Y\) be a morphism that induces a bijection \(X(*)\to Y(*)\).
    Then \(f\) is already an isomorphism.
\end{lem}
\begin{proof}
    Since \(\ker(f)\) and \(\coker(f)\) are similarly such sequential colimits it suffices to check that if \(X(*)=0\) then \(X=0\).
    Let \(X=\colim_n X_n\) where \(X_n\) come from compact Hausdorff abelian groups.
    Define \(\overline{X}_n\defined\im(X_n\to X)\).
    Then \(X=\colim_n\overline{X}_n\) with \(\overline{X}_n(*)=0\) and \(\overline{X}_n=\colim_m\im(X_n\to X_m)\).
    In this system all transition maps are surjective.
    Thus we may assume that all transition maps \(X_n\to X_{n+1}\) are surjective.
    Then \(X_n=X_0/K_n\) for some closed subgroup \(K_n\) and \(X_0=\bigcup_{n\geq 0}K_n\) and \(K_n\subseteq K_{n+1}\).
    By \cref{lem: baire application} we find that \(X_0=K_n\) for some \(n\) so \(X_n=0\) for some \(X_n\), hence \(X=0\).
\end{proof}
\begin{prop}[\cref{prop: transfer condensed iso}]
    The transfer map $H_1(W_{F/E},\widehat{L})\to H_1(F^\times,\widehat{L})^{Q}$ is an isomorphism of condensed abelian groups.
\end{prop}
\begin{proof}
    By \cref{lem: sequential colimit qcqs condensed on underlying set} it suffices to show that both sides come from sequential colimits of compact Hausdorff abelian groups.
    This is clear for \(H_1(F^\times,\widehat{L})^{Q}=\cond{T(E)}\).
 
    The inflation-restriction sequence on condensed group homology gives us an exact sequence of condensed abelian groups
    \begin{equation*}
        H_2(Q,\widehat{L})\xto{\delta} H_1(F^\times,\widehat{L})_{Q}\to H_1(W_{F/E},\widehat{L})\to H_1(Q,\widehat{L})\to 0
    \end{equation*}
    Note that \(H_1(F^\times,\widehat{L})\to H_1(F^\times,\widehat{L})_{Q}\) is surjective with kernel \(\sum_{\sigma\in Q}\im(\sigma-1)\), so every term comes from a sequential colimit of compact Hausdorff abelian groups.
    It follows from \cref{lem: sequential colimit Hausdorff closed under extension finite (co)limits} that then \(H_1(W_{F/E},\widehat{L})\) is also such a colimit.
\end{proof}