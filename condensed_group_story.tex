\section{Condensed group (co)homology}\label{condensed group (co)homology}
In this section we develop the necessary amount of condensed group (co)homology to formulate what transfer is in the condensed setting.
For this entire section, let \(G\) be a condensed group, \(H\) a condensed subgroup, \(M\) a condensed abelian group with a linear \(G\)-action and \(S\) an extremally disconnected set.
\begin{defn}
    Define the \defword{condensed group homology} respectively \defword{condensed group cohomology} to be 
    \begin{align*}
        H_i(G,M)&\defined\pi_i(\bbZ\lotimes_{\bbZ[G]}M)\\
        \shortintertext{respectively}
        H^i(G,M)&\defined\pi_{-i}\riHom_{\bbZ[G]}(\bbZ,M)
    \end{align*}
    where \(\bbZ[G]\) is the sheafification of the presehaf \(S\mapsto \bbZ[G(S)]\).
    We will also write \(M_G\) for \(H_0(G,M)\) and \(M^G\) for \(H^0(G,M)\).
\end{defn}
\begin{rem}
    Note that 
    \begin{align*}
        \bbZ\lotimes_{\bbZ[H]}M&\cong \bbZ[G/H]\lotimes_{\bbZ[G]}M\\    
        \shortintertext{and}
        \riHom_{\bbZ[H]}(\bbZ,M)&\cong\riHom_{\bbZ[G]}(\bbZ[G/H],M)
    \end{align*}
    In particular, if \(G/H\) is a condensed group, the condensed group (co)homology carries a residual \(G/H\)-action.
\end{rem}
\begin{rem}\label{rem: underlying set condensed homology is ordinary homology}
    Since evaluation on extremally disconnected sets is strong monoidal and exact we see that
    \begin{equation*}
        (\bbZ\lotimes_{\bbZ[G]}M)(S)=\bbZ(S)\lotimes_{\bbZ[G](S)}M(S)
    \end{equation*}
    so in particular \(H_i(G,M)(*)=H_i(G(*),M(*))\).
\end{rem}
\begin{constr}
    Let \(P_\bullet(S)\to\bbZ(S)\) be the bar resolution for \(G(S)\).
    Then the sheafification of \(S\mapsto P_\bullet(S)\) is a flat resolution of \(\bbZ\) with \(P_n=\bbZ[G][G^n]\), which we will also call the bar resolution.
\end{constr}
\begin{rem}\label{rem: bar resolution not projective}
    The bar resolution is not generally a projective resolution.
    Therefore it is generally unsuited to compute condensed group cohomology.
    The situation is better when \(G\) comes from a profinite group and \(M\) is solid as a condensed abelian group.
    We return to this at the end of this section.
\end{rem}
\begin{rem}
    If \(G\) is a discrete abstract group, then the bar resolution is just the ordinary bar resolution with the discrete condensed structure.
    It follows that 
    \begin{equation*}
        H_i(G,M)=\cond{H_i(G(*),M(*))}
    \end{equation*}
    whenever \(M\) is a discrete \(G\)-module.
\end{rem}
\begin{defn}
    Let \(N\) be a condensed \(H\)-module.
    Define the \defword{coinduction} respectively \defword{induction} by
    \begin{align*}
        \ind_H^G(N)\defined \bbZ[G]\otimes_{\bbZ[H]}N
        \shortintertext{respectively}
        \coind_H^G(N)\defined\iHom_{\bbZ[H]}(\bbZ[G],N)
    \end{align*}
    The coinduction is naturally a right \(G\)-module, but we will consider it as a left \(G\)-module by precomposing with the inversion on \(G\).
\end{defn}
\begin{lem}
    We have adjunctions
    \begin{equation*}
        \ind_H^G\dashv\res_H^G\dashv\coind_H^G
    \end{equation*}
\end{lem}
\begin{proof}
    Clearly there is an adjunction on the presheaf versions of these functors.
    One can just sheafify that.
    % Given a map \(f\from\ind_H^G(N)\to M\) we get a map \(f'\from N\to \res_H^G M\) by sending \(g\otimes m\in \bbZ[G](S)\otimes_{\bbZ[H](S)}M(S)\) to \(gf(m)\).
    % Given a map \(f\from\res_H^GM\to N\) we get a map \(f'\from M\to\coind_H^G N\) by sending \(m\in M(S)\) to the map sending \(g\otimes d\in\bbZ[G](T)\otimes_{\bbZ(S)}\bbZ[S](T)\) to \(d^*f(g.m)|_{T}\). %todo or similar, check coinduction
\end{proof}
\begin{lem}\label{lem: Z[G] as Z[H] module}
    Assume that \(G/H\) is a finite discrete set.
    A set-theoretic section \(s\) of \(G(*)\to G/H(*)\) gives us isomorphisms \(\bbZ[H][G/H]\cong \bbZ[G]\) and \(\bbZ[H][H\backslash G]\cong \bbZ[G]\) as right respectively left \(\bbZ[H]\)-modules.
\end{lem}
\begin{proof}
    We will do the case for right \(\bbZ[H]\)-modules.
    The other case is similar.
    We easily see that \(\bbZ[H][G/H]\cong\bbZ[H\times G/H]\) as right \(\bbZ[H]\)-modules, where \(H\) acts on \(H\times G/H\) naturally on the first factor and trivially on the second.
    Thus we need to check that \(H\times G/H\cong G\) as condensed sets with \(H\)-action.
    First note that \(s\from G/H(*)\to G(*)\) is continuous with respect to the topologies on both sides coming from the condensed structure.
    By adjunction we get a morphism of condensed sets \(s\from\cond{G/H(*)}\to G\).
    By assumption \(\cond{G/H(*)}=G/H\).
    We define \((H\times G/H)(S)\to G(S)\) by \((h,\overline{g})\to s(\overline{g}h)\) and \(G(S)\to (H\times G/H)(S)\) by \(g\mapsto (s(\overline{g}^{-1})g,\overline{g})\) where \(\overline{g}\) is the image of \(g\) under \(G\to G/H\).
    These are inverse maps and \(H\)-linear.
\end{proof}
\begin{lem}[Shapiro's lemma]
    We have a canonical isomorphism
    \begin{align*}
        H_*(H,N)\xto{\cong} H_*(G,\ind_H^G N)
    \end{align*}
    If \(\bbZ[G]\) is projective over \(\bbZ[H]\) (for example when \(G/H\) comes from a finite discrete set, by \cref{lem: Z[G] as Z[H] module}), then
    \begin{equation*}
        H^*(G,\coind_H^G N)\xto{\cong}H^*(H,N)
    \end{equation*}
\end{lem}
\begin{proof}
    This is clear for homology, since
    \begin{equation*}
        H_*(G,\ind_{H}^G N)\cong H_*(P_{\bullet}\otimes_{\bbZ[G]}\bbZ[G]\otimes_{\bbZ[H]}M)\cong H_*(P_{\bullet}\otimes_{\bbZ[H]}N)
    \end{equation*}
    where \(P_{\bullet}\) is the bar resolution for \(\bbZ\) as a \(\bbZ[G]\)-module.
    Note that \(\bbZ[G]\) is flat as a \(\bbZ[H]\)-module, as this is clearly true on the level of presheaves.
    If \(\bbZ[G]\) is projective over \(\bbZ[H]\), then 
    \begin{equation*}
        \riHom_{\bbZ[G]}(\bbZ,\coind_H^GN)\cong\riHom_{\bbZ[G]}(\bbZ,\riHom_{\bbZ[H]}(\bbZ[G],N))\cong\riHom_{\bbZ[H]}(\bbZ,N)
    \end{equation*}
    which implies the claim for cohomology.
\end{proof}
\begin{lem}
    Assume that \(G/H\) is a finite discrete set.
    Then we have an \(G\)-equivariant isomorphism
    \begin{equation*}
        \ind_H^G N\cong\coind_H^G N
    \end{equation*}
\end{lem}
\begin{proof}
    We have
    \begin{align*}
        \ind_H^GN&\cong\bbZ[G]\otimes_{\bbZ[H]}N\\
        &\cong\bbZ[G/H]\otimes_{\bbZ} N\\
        &\cong\bigoplus_{s\in G/H}N\\
        &\cong\bigoplus_{s\in H\backslash G}N\\
        &\cong\iHom_{\bbZ}(\bbZ[H\backslash G],N)\\
        &\cong\iHom_{\bbZ[H]}(\bbZ[G],N)
    \end{align*}
    Note that while the third and fifth isomorphism depends on a choice of set-theoretic section to \(G(*)\to G/H(*)\), the entire composite is independent of this choice.
\end{proof}
\begin{defn}\label{def: transfer condensed group (co)homology}
    Assume that \(G/H\) is a finite discrete set.
    We define transfer maps in condensed group (co)homology by setting
    \begin{align*}
        \tr^G_H&\from H_*(G,M)\to H_*(G,\coind^G_H\res^G_HM)\xto{\cong}H_*(G,\ind^G_H\res^G_H M)\xto{\cong}H_*(H,\res^G_H M)\\
        \tr^G_H&\from H^*(H,M)\xto{\cong} H^*(G,\coind^G_H\res^G_HM)\xto{\cong}H^*(G,\ind^G_H\res^G_H M)\to H^*(H,\res^G_H M)
    \end{align*}
    The isomorphisms are Shapiro's lemma and the previous lemma, the other arrows are the unit or counit of the coinduction, restriction, induction adjunction.
\end{defn}
\begin{lem}\label{lem: functoriality of transfer}
    Assume that \(H'\) is a normal subgroup of \(H\) and \(G\).
    Assume that \(M\) comes from a \(G/H'\)-module.
    Then the following diagram commutes:
    \begin{equation*}
        \begin{tikzcd}
            {H_*(G,M)} \arrow[d] \arrow[r, "\tr^G_H"]    & {H_*(H,M)} \arrow[d] \\
            {H_*(G/H',M)} \arrow[r, "\tr_{H/H'}^{G/H'}"] & {H_*(H/H',M)}       
            \end{tikzcd}
    \end{equation*}
\end{lem}
\begin{proof}
    We have maps
    \begin{align*}
        \coind^G_HM=\iHom_{\bbZ[H]}(\bbZ[G],M)\cong\iHom_{\bbZ[H/H']}(\bbZ[G]\otimes_{\bbZ[H]}\bbZ[H/H'],M)=\coind_{H/H'}^{G/H'}M\\
        \shortintertext{and}    
        \ind^G_H M=\bbZ[G]\otimes_{\bbZ[H]}M\to\bbZ[G/H']\otimes_{\bbZ[H]}M\cong\bbZ[G/H']\otimes_{\bbZ[H/H']}M=\ind^{G/H'}_{H/H'}M
    \end{align*}
    where \(\bbZ[G/H']\otimes_{\bbZ[H]}M\cong\bbZ[G/H']\otimes_{\bbZ[H/H']}M\) follows from the corresponding statement on the presheaf level.
    These fit into the following commuting diagram:
    \begin{equation*}
        \begin{tikzcd}[column sep=small]
            {H_*(G,M)} \arrow[d] \arrow[r] & {H_*(G,\coind^G_HM)} \arrow[d] \arrow[r]    & {H_*(G,\ind^G_HM)} \arrow[d] \arrow[r] & {H_*(H,M)} \arrow[d] \\
            {H_*(G/H',M)} \arrow[r]        & {H_*(G/H',\coind^{G/H'}_{H/H'}M)} \arrow[r] & {H_*(G,\ind^{G/H'}_{H/H'}M)} \arrow[r] & {H_*(H/H',M)}       
            \end{tikzcd}
    \end{equation*}
    The top horizontal arrows define \(\tr^G_H\) and the bottom horizontal arrows define \(\tr^{G/H'}_{H/H'}\).
\end{proof}
\begin{rem}\label{rem: transfer and G-action}
    Observe that in the setting of the \cref{def: transfer condensed group (co)homology}, that for \(g\in G(*)\) we get commuting triangles
    \begin{equation*}
        \begin{tikzcd}
            & {H_*(G,M)} \arrow[ld, "\tr^G_H"'] \arrow[rd, "\tr^G_{gHg^{-1}}"] &                   \\
            {H_*(H,M)} \arrow[rr, "g.(-)"] &                                                                & {H_*(gHg^{-1},M)}
        \end{tikzcd}
    \end{equation*}
    \begin{equation*}
        \begin{tikzcd}
            & {H^*(G,M)} &                                                                      \\
            {H^*(H,M)} \arrow[ru, "\tr^G_H"] &            & {H^*(gHg^{-1},M)} \arrow[ll, "g.(-)"'] \arrow[lu, "\tr^G_{gHg^{-1}}"']
            \end{tikzcd}
    \end{equation*}
\end{rem}
\begin{lem}\label{lem: homology inclusion is norm map}
    Assume that \(G'\) is a subgroup of \(G\) such that any quotient of the tower \(H\subset G'\subset G\) is finite discrete and \(H\) is a normal subgroup of \(G'\) and \(G\).
    Then the following diagram commutes:
    \begin{equation*}
        \begin{tikzcd}
            {H_*(G',M)} \arrow[d] \arrow[r, "\tr^{G'}_H"] & {H_*(H,M)} \arrow[d, "\sum_{g\in G/G'(*)}g.(-)"] \\
            {H_*(G,M)} \arrow[r, "\tr^G_H"]               & {H_*(H,M)}                                      
            \end{tikzcd}
    \end{equation*}
    Note the implicit choice of coset representatives for \(G/G'(*)\).
\end{lem}
\begin{proof}
    By dimension shifting we reduce to the case where \(*=0\).
    Then as \(\bbZ[G']\)-modules we have \(\coind^G_H M=\iHom_{\bbZ[H]}(\bbZ[G],M)=\iHom_{\bbZ[H]}(\bbZ[G'][G/G'],M)\), once given a choice of coset representatives \(g_i\in G/G'(*)\).
    Unwinding, %todo
    we see that the following diagram of \(\bbZ[G']\)-modules commutes:
    \begin{equation*}
        \begin{tikzcd}
            & \coind^{G'}_H M \arrow[dd, "\sum g_i.(-)"] \\
M \arrow[ru] \arrow[rd] &                                            \\
            & \coind^G_HM=\bigoplus_{s\in G/G'(*)} M    
\end{tikzcd}
    \end{equation*}
    From this the claim follows. %todo cleanup
\end{proof}
\begin{rem}
    By functoriality of the Lyndon-Hochschild-Serre spectral sequence we can extend it to condensed group homology and condensed group cohomology.
    Assume that \(H\) is a normal subgroup of \(G\), then we have the following 5-term exact sequence:
    \begin{equation*}
        H_2(G,M)\to H_2(G/H,M_H)\to H_1(H,M)_{G/H}\to H_1(G,M)\to H_1(G/H,M_H)\to 0
    \end{equation*}
\end{rem}
As observed in \cref{rem: bar resolution not projective}, we cannot use the bar resolution to compute condensed group cohomology in general.
The next observation will be crucial in the following.
\begin{custom}{Definition/Lemma}
    There is an analytic animated ring structure \(\mathcal{M}\) on \(\bbZ[G]\) such that the \((\bbZ[G], \mathcal{M})\)-modules are the \(\bbZ[G]\)-modules whose underlying abelian group is solid.
    For a profinite set \(S\) we have \(\mathcal{M}[S]=\bbZ[G][S]^{L\solid}\).
    Let us call this analytic ring \(\bbZ[G]_{\solid}\).
\end{custom}
\begin{proof}
    Using the characterization of \cite[Proposition 12.20.]{analytic}, it is immediate that such an \(\mathcal{M}\) must exist (the condensed internal hom of solid modules is solid, since \(\bbZ_{\solid}\) is an analytic ring).
    The inclusion \(\D((\bbZ[G],\mathcal{M}))\subset\D(\bbZ[G])\) has derived solidification as left adjoint, which follows from the corresponding fact about solid abelian groups.
    It follows that \(\mathcal{M}[S]\) is given by the derived solidification of \(\bbZ[G][S]\).
\end{proof}
\begin{rem}
    If \(G\) (as a condensed set) is a filtered colimit of  profinite sets, then \(\bbZ[G][S]^{L\solid}=\bbZ[G][S]^{\solid}\) and \(\bbZ[G]_{\solid}\) is an analytic ring.
    In particular \(\D(\bbZ[G]_{\solid})\) is the derived category of its heart.
    To see that the derived solidification is concentrated in degree 0 write \(G=\colim S_i\).
    Then 
    \begin{equation*}
        \bbZ[G][S]=\bbZ[G\times S]=\bbZ[\colim S_i\times S]=\colim\bbZ[S_i\times S]
    \end{equation*}
    Therefore
    \begin{align*}
        (\colim\bbZ[S_i\times S])^{L\solid}&=\colim(\bbZ[S_i\times S]^{L\solid})\\
        &=\colim\bbZ[S_i\times S]^{\solid}\\
        &=\bbZ[G][S]^{\solid}
    \end{align*}
    Note that each \(\bbZ[S_i\times S]\) is concentrated in degree 0 by \cite[Corollary 6.1.(iv)]{condensed}.
    In particular this applies when \(G\) comes from a locally profinite group.
\end{rem}
Let us from now on assume that \(G\) comes from a profinite group and that \(M\) is solid as a condensed abelian group.
%Then we will show that the bar resolution computes \(H^i(G,M)\).
% \begin{lem}
%     For a profinite set \(S\) the \(G\)-module \(\bbZ[G][S]\) is acyclic for \(\iHom_{\bbZ[G]}(-,M)\).
% \end{lem}
% \begin{proof}
%     We need to show that \(\riHom_{\bbZ[G]}(\bbZ[G][S],M)\) is concentrated in degree 0.
%     First obsere that \(\bbZ[G]\) is \(\bbZ\)-flat, so we have \(\bbZ[G][S]=\bbZ[G]\lotimes_{\bbZ}\bbZ[S]\).
%     It follows by derived extension of scalars that \(\riHom_{\bbZ[G]}(\bbZ[G][S],M)=\riHom_{\bbZ}(\bbZ[S],M)\).
%     Since \(M\) is solid this agrees with \(\riHom_{\bbZ}(\bbZ[S]^\solid,M)\).
%     We know that \(\bbZ[S]^\solid=\prod_{i\in I}\bbZ\) for some indexing set \(I\) by \cite[Corollary 5.5.]{condensed} and that condesed abelian groups like this are projective as solid abelian groups, see \cite[Corollary 6.1.(i)]{condensed}.
%     Therefore \(\riHom(\bbZ[S]^\solid,M)\) is concentrated in degree 0.
% \end{proof}
\begin{cor}\label{cor: bar resolution computes condensed profinite group cohomology}
    Let \(P_\bullet\to \bbZ\) be the bar resolution.
    Then \(\iHom_{\bbZ[G]}(P_\bullet,M)\) computes \(H^i(G,M)\).
\end{cor}
\begin{proof}
    Each \(G^n\) is profinite, so the base change \(P_{\bullet}\otimes_{\bbZ[G]}\bbZ[G]_{\solid}=\bbZ[G]_{\solid}[G^\bullet]\) is a resolution of \(\bbZ\otimes_{\bbZ[G]}\bbZ[G]_{\solid}=\bbZ\) by projective \(\bbZ[G]_{\solid}\)-modules.
    For projectivity observe that on \(\bbZ[G]_{\solid}\)-modules we have 
    \begin{align*}
        \Hom_{\bbZ[G]_{\solid}}(\bbZ[G][G^n]^\solid,-)&=\Hom_{\bbZ[G]}(\bbZ[G][G^n],-)\\
        &=\Hom_{\bbZ}(\bbZ[G^n],-)\\
        &=\Hom_{\bbZ}(\bbZ[G^n]^\solid,-)
    \end{align*}
    Now \(\bbZ[G^n]^\solid\) is a product of \(\bbZ\) by \cite[Corollary 5.5.]{condensed} and these are projective solid abelian groups \cite[Corollary 6.1.(i)]{condensed}, so the last hom-functor is exact.
    The claim follows by the adjunction isomorphism \(\iHom_{\bbZ[G]}(P_\bullet,M)\cong\iHom_{\bbZ[G]}(P_\bullet\otimes_{\bbZ[G]}\bbZ[G]_{\solid},M)\).
\end{proof}
\begin{cor}\label{cor: condensed profinite group cohomology agrees with classical}
    Let \(M\) be a discrete abelian group.
    Then we have 
    \begin{equation*}
        H^i(G,M)(*)=H^i_{\mathrm{cont}}(G,M)
    \end{equation*}
    where the right hand side denotes the classical continuous group cohomology of profinite groups.
\end{cor}
\begin{proof}
    Discrete abelian groups are solid, since \(\bbZ\) is solid and solid abelian groups are closed under colimits, colimits of discrete abelian groups stay discrete by \cref{lem: condensation commutes with colimits of discrete modules} and discrete abelian groups embed fully faithfully into condensed abelian groups.
    We know that \(\iHom_{\bbZ[G]}(P_\bullet,M)\) computes \(H^i(G,M)\), where \(P_\bullet\to \bbZ\) is the bar resolution.
    Using adjunctions we have \(\iHom_{\bbZ[G]}(P_\bullet,M)\cong\iHom(G^{\bullet},M)\) as complexes.
    Since evaluation at a point is exact and \(G^n\) as well as \(M\) come from compactly generated Hausdorff spaces we see that \(\iHom(G^{\bullet},M)\cong C(G^\bullet,M)\).
    The right hand side is precisely the complex of continuous cochains that computes continuous profinite group cohomology.
\end{proof}