\section{Geometric incarnation of Langlands duality for tori}
\begin{lem}\label{lem: moduli of condensed cocycles representable}
    Let \(W\) be a topological group that is locally compact and weak Hausdorff, \(G\) a linear algebraic group with action of \(W\) that factors over a discrete group \(V\).
    Let \(P\subset W\) be a pro-\(p\) subgroup of \(W\) such that \(P\) acts trivially on \(G\).
    Assume that for all open subgroups \(P'\subset P\) there is a scheme \(Z^1(W/P',G)\) over \(\zl\) parametrizing condensed 1-cocycles \(W/P'\to G(\Lambda)\) for \(\Lambda\) a \(\zl\)-algebra that is condensed via \(\Lambda_{\mathrm{disc}}\otimes_{\bbZ_{\ell,\mathrm{disc}}}\zl\).
    Then there is a scheme \(Z^1(W,G)\) parametrizing condensed 1-cocycles and \(Z^1(W/P',G)\to Z^1(W,G)\) is the inclusion of an open and closed subscheme if \(P'\) is a normal subgroup of \(P\).
\end{lem}
\begin{proof}
    This essentially follows from the proof of \cite[Theorem VIII.1.3.]{geometrization}.
    Let us supply more details.
    It follows from the assumptions that \(W\) is compactly generated.
    Let \(N\defined\ker(W\to V)\), let us fix coset representatives \(v\in W\) for \([v]\in V=W/N\).
    Note that \(P\subset N\).
    A condensed cocycle \(\varphi\from\cond{W}\to G(\Lambda)\) is determined by the restriction \(\cond{N}\to G(\Lambda)\) and the images of \(v\) for \([v]\in V\).
    This is because given \(f\in C(S,W)\) for \(S\) an extremally disconnected set, we may assume that composing with \(W\to V\) we get a constant function with some value \([v]\in V\) after localizing (this uses that \(V\) is discrete).
    Then \(\im(f)\in vN\), thus \(\varphi(f)\) is uniquely determined by the above data.
    Choose an embedding \(G\injto\GL_n\).
    Then \(N\to G(\Lambda)\) gives us a map \(N\to\GL_n(\Lambda)\) by postcomposing.
    Such a map is equivalent to a \(\Lambda\)-linear condensed action \(\cond{N}\times\Lambda^n\to\Lambda^n\).
    Here everything comes from maps of compactly generated weak Hausdorff spaces and \(N\) is locally compact.
    This shows \(\cond{N}\times\Lambda^n=\cond{N\times\Lambda_{\ladic}^n}\).
    It follows from \cite[Theorem 2.16.(ii)]{condensed} that such condensed actions are equivalent to \(\Lambda\)-linear continuous actions \(N\times\Lambda_{\ladic}^n\to\Lambda_{\ladic}^n\).
    Here we use the fact that since \(N\) is locally compact, the product in compactly generated spaces agrees with the product in topological spaces.
    When we restrict the action to \(P\subset N\) it has open stabilizers by \cref{lem: continuous action of locally pro p on ladic}.
    Therefore there is some open \(P'\subset P\) such that \(\cond{N}\to\GL_n(\Lambda)\) factors over \(\cond{N/P'}\) and for this \(P'\) the condensed cocycle \(\cond{W}\to G(\Lambda)\) factors over \(\cond{W/P'}\).
    It follows that 
    \begin{equation*}
        Z^1(W,G)=\colim_{P'\subset P}Z^1(W/P',G)
    \end{equation*}
    with \(P'\) running over open subgroups of \(P\).
    Since \(P\) is pro-\(p\) we may also assume that the \(P'\) are normal subgroups.
    By assumption it suffices to check that for \(P''\subset P'\) open normal subgroups of \(P\) we have that the induced map \(Z^1(W/P',G)\to Z^1(W/P'',G)\) is open and closed.
    Let \(\gamma\in P'/P''\).
    We have \(\gamma^{p^r}=1\) for some \(r>0\), since \(P'/P''\) is a \(p\)-group.
    Taking a closed embedding \(G\injto \GL_n\) the locus where \(A=1\) is a connected component of the locus of all \(A\in\GL_n\) such that \(A^{p^n}=1\), as can be checked by observing that the tangent space at \(A=1\) is trivial.
    It follows that the locus of those condensed cocycles \(\varphi\from W/P''\to G(\Lambda)\) where \(\varphi(\gamma)=1\) is open and closed, being the preimage of \(1\) for the evaluation at \(\gamma\) map.
    \(Z^1(W/P')\) is the locus where \(\varphi(\gamma)=1\) for all \(\gamma\in P'/P''\).
    Since \(P'/P''\) is finite it follows that \(Z^1(W/P',G)\to Z^1(W/P'',G)\) is the inclusion of an open and closed subscheme.
    Therefore the colimit of the \(Z^1(W/P',G)\) exists as a scheme and each  \(Z^1(W/P',G)\to Z^1(W,G)\) is an open and closed immersion. 
\end{proof}
\begin{rem}
    Using the proof we can show that \(Z^1(W_{F/E},\widehat{T})\cong Z^1(W_E,\widehat{T})\).
    From now on we will freely use this isomorphism, depending on what is notationally advantageous.
\end{rem}
\begin{cor}
    Both \(Z^1(W_{F/E},\widehat{T})\) and \(\Hom(T(E),\gm)\) are representable.
    If \(P\subset F^\times\) is open then \(Z^1(W_{F/E}/P,\widehat{T})\) is an open and closed subscheme of \(Z^1(W_{F/E},\widehat{T})\).
    The same result holds for \(\Hom(T(E)/K,\gm)\to\Hom(T(E),\gm)\) when \(K\) is a pro-\(p\) subgroup of \(T(E)\).
\end{cor}
\begin{lem}\label{lem: units fpqc locally divisible}
    Let \(\Lambda\) be a ring.
    Then there is a ring \(\Lambda'\) with a faithfully flat ring map \(\Lambda\to\Lambda'\) such that \({\Lambda'}^\times\) is divisible.
\end{lem}
\begin{proof}
    Let \(\Lambda_{i+1}\defined\Lambda_i[a^{1/n}\mid a\in\Lambda_i^\times,n\in\bbN]\) and \(\Lambda_0\defined\Lambda\).
    Then set \(\Lambda'\defined\colim_{i\geq 0}\Lambda_i\).
    Since filtered colimits of rings are computed on the underlying abelian group every unit of \(\Lambda'\) is divisible.
    For all \(i\geq 0\) the ring morphism \(\Lambda_i\to\Lambda_{i+1}\) has a retract as a \(\Lambda\)-module map and makes \(\Lambda_{i+1}\) a free \(\Lambda\)-module, as the map \(\Lambda_i\to\Lambda_{i+1}\) is base changed from \(\bbZ[t_i\mid i\in I]\to\bbZ[t_i^{1/n}\mid i\in I,n\in\bbN]\) for any indexing set \(I\).
    Then \(\bbZ[t_i^{1/n}\mid i\in I,n\in\bbN]\) is a free \(\bbZ[t_i\mid i\in I]\)-module with basis \(\prod_{j\in J}t_j^{m_j/n_j}\) with \(J\subset I\) finite and \(m_j<n_j\).
    The map \(\bbZ[t_i\mid i\in I]\to\bbZ[t_i^{1/n}\mid i\in I,n\in\bbN]\) is injective with image being the subspace spanned by \(1\), so it the inclusion of a direct summand.
    It follows that \(\Lambda'\) and \(\Lambda_{i+1}/\Lambda_i\) are flat \(\Lambda\)-modules. 
    Let \(N\neq 0\) be a \(\Lambda\)-module.
    For faithful flatness we need to check that \(\Lambda'\otimes_{\Lambda}N\neq 0\).
    For this observe that \(\Lambda_i\otimes_{\Lambda}N\to\Lambda_{i+1}\otimes_{\Lambda}N\) is injective, so \(\Lambda'\otimes_{\Lambda}N\) is given by a sequential colimit of non-zero modules with injective transition maps.
    Such colimts are non-zero.
\end{proof}
\begin{lem}
    The stack \([Z^1(W_E,\widehat{T})/\widehat{T}]\) is a gerbe over an algebraic space.
\end{lem}
\begin{proof}
    Using \cite[Tag 06PB]{stacks} we can easily compute that the inertia stack is 
    \begin{equation*}
        \widehat{T}^Q\times[Z^1(W_E,\widehat{T})/\widehat{T}]\to[Z^1(W_E,\widehat{T})/\widehat{T}]
    \end{equation*}
    where the map is the projection.
    The claim follows from \cite[Tag 06QJ]{stacks}.
\end{proof}
Recall that for an algebraic stack \(\mathcal{X}\) that is a gerbe over some algebraic space \(X\) that \(X\) is unique and is given by the fppf-sheafification of \(\pi_0 \mathcal{X}\) (\cite[Tag 06QD]{stacks}), which is also then the coarse moduli space.
Since algebraic spaces are fpqc-sheaves (\cite[Tag 0APL]{stacks}) one can also consider the fpqc-sheafification.
\begin{lem}\label{lem: geometric incarnation langlands duality tori}
    We have a morphism of stacks 
    \begin{equation*}
        \Psi\from [Z^1(W_{F/E},\widehat{T})/\widehat{T}]\to\Hom(T(E),\gm)
    \end{equation*}
    that exhibits \([Z^1(W_{F/E},\widehat{T})/\widehat{T}]\) as a gerbe over an algebraic space.
    Running over \(P\subset F^\times\) open such that \(\Theta(P)\) is pro-\(p\), it is induced from a map 
    \begin{equation*}
        \Psi_P\from Z^1(W_{F/E}/P,\widehat{T})\to \Hom(T(E)/\Theta(P),\gm)
    \end{equation*}
    that sends a cocycle \(f\from W_{F/E}/P\to\widehat{T}(\Lambda)\) to
    \begin{equation*}
        \sum m_i\otimes [g_i]\mapsto\prod f(g_i)(m_i)
    \end{equation*}
    for \(\sum m_i\otimes [g_i]\in H_1(W_{F/E}/P,\widehat{L})\) and we identify \(H_1(W_{F/E}/P,\widehat{L})\) with \(T(E)/\Theta(P)\) using \cref{prop: transfer condensed iso}.
    For an alternative perspective, there is a natural pairing 
    \begin{equation*}
        H^1(W_{F/E}/P,\Hom(\widehat{L},\Lambda^\times))\times H_1(W_{F/E}/P,\widehat{L})\to H_0(W_{F/E}/P,\Lambda^\times)=\Lambda^\times
    \end{equation*}
    and the map above is the one induced by this pairing.
\end{lem}
\begin{proof}
    % When \(X\) is a scheme and \(G\) a group scheme acting on \(X\) we let \([X/_p G]\) denote the prestack quotient: it sends a ring \(\Lambda\) to the action groupoid \(X(\Lambda)//G(\Lambda)\).
    % Observe that \(\pi_0([Z^1(W_E,\widehat{T})/_p\widehat{T}])=H^1(W_{F/E},\widehat{T})\).
    % Note that on objects \(\Psi\) factors through this, as it comes from the pairing \(H^1(W_{F/E},\Hom(\widehat{L},\Lambda))\times H_1(W_{F/E},\widehat{L})\to H_0(W_{F/E},\Lambda)=\Lambda\).
    % Observe that if \(f\from W_{F/E}\to\widehat{T}(\Lambda)\) is a condensed cocycle, then the \(t\in \widehat{T}(\Lambda)\) such that \(tft^{-1}=f\) is given by \(\widehat{T}^Q(\Lambda)\).
    % In general if we have another condensed cocycle \(f'\from W_{F/E}\to\widehat{T}(\Lambda)\) the space of \(t\in\widehat{T}(\Lambda)\) such that \(tft^{-1}=f'\) is a torsor under \(\widehat{T}^Q(\Lambda)\).
    % When \(tft^{-1}=f'\) holds we will also write \(t\from f\to f'\).
    % Note that by our previous observations that \(\Psi(f)=\Psi(f')\), so to define \(\Psi(t\from f\to f')\) it will suffice to find a preferred element in this \(\widehat{T}^Q(\Lambda)\) torsor that depends functorially on \(\Lambda\).
    % For this it suffices to find a split of the projection \(\widehat{T}\to\widehat{T}/\widehat{T}^Q\).
    % Then a given \(t\from f\to f'\) is naturally an element of \(\widehat{T}/\widehat{T}^Q\), and a lift to \(\widehat{T}\) will be a preferred element that is functorial in \(\Lambda\).
    % First observe that \(\widehat{T}/\widehat{T}^Q\) is a diagonalizable group with character group \(\ker(\widehat{L}\to\widehat{L}_Q)\).
    % This shows that the character group is free and thus \(\widehat{T}/\widehat{T}^Q\) is a torus.
    % Since this everything is defined over \(\bbZ\) this is split.
    % Next observe that \(\widehat{T}\to\widehat{T}/\widehat{T}^Q\) is an fppf torsor for \(\widehat{T}^Q\).
    % We have a short exact sequence of fppf sheaves 
    % \begin{equation*}
    %     0\to \widehat{T}^Q\to\widehat{T}\to\widehat{T}/\widehat{T}^Q\to 0
    % \end{equation*}
    % Since \(\widehat{T}/\widehat{T}^Q\) and \(\widehat{T}\) are split tori we have \(H^1_{\mathrm{fppf}}(\widehat{T}/\widehat{T}^Q,\widehat{T})=0\) (by descent this agrees with the Zariski cohomology, which is 0 as unique factorization domains have trivial Picard group).
    % Thus 
    % \begin{equation*}
    %     H^1_{\mathrm{fppf}}(\widehat{T}/\widehat{T}^Q,\widehat{T}^Q)=\coker(H^0(\widehat{T}/\widehat{T}^Q,\widehat{T})\to H^0(\widehat{T}/\widehat{T}^Q,\widehat{T}/\widehat{T}^Q))
    % \end{equation*}
    % This vanishes as \(\widehat{T}/\widehat{T}^Q\) is affine, so \(H^0\) on it is exact.
    % We have shown that all fppf torsors for \(\widehat{T}^Q\) on \(\widehat{T}/\widehat{T}^Q\) split, thus we find a section.
    % Now that \(\Psi\) is well-defined, we need to show that it is an isomorphism.
    % Let us start with showing that it is a bijection on \(\pi_0\).
    % Replacing \(W_{F/E}\) with \(W_{F/E}/P\) and \(T(E)\) with \(T(E)/\Theta(P)\) for \(P\subset F^\times\) a Galois-stable open subgroup allows us to stay within the world of non-condensed mathematics.
    % Observe that both stacks are in fact fpqc stacks as they are quotient stacks by fppf group schemes.
    % The main point is that \(\pi_0\Psi\from H^1(W_{F/E}/P,\widehat{T}(\Lambda))\xto{\cong} \Hom(H_1(W_{F/E}/P,\widehat{L}),\Lambda^\times)\) whenever \(\Lambda^\times\) is an injective abelian group or more generally any injective abelian group (where \(\widehat{T}(A)\defined\Hom(\widehat{L},A)\) for \(A\) an abelian group).
    % For general \(\Lambda\), let \(l\from\Lambda\to\Lambda'\) be a faithfully flat map as in \cref{lem: units fpqc locally divisible}.
    % Let \(f\from W_{F/E}/P\to\widehat{T}(\Lambda)\) such that \(\pi_0\Psi(f)=0\).
    % Then \(\pi_0\Psi(l\circ f)=0\).
    % The image of \(l\circ f\) is \(l(\Lambda^\times)\) which is divisible, so \(l\circ f=0\).
    % This means fpqc-locally there is a \(t\from f\to 0\).
    % This shows injectivity.
    % For surjectivitiy let \(g\from H_1(W_{F/E},\widehat{L})\to\Lambda^\times\).
    % Then \(l\circ g\) lies in the image of \(\pi_0\Psi\) by the same argument, thus fpqc-locally everything gets hit by \(\pi_0\Psi\).
    % We need to show fully faithfulness.
    % By our previous calculation on \(\pi_0\) it suffices to show that \(\Psi\) induces a bijection on the endomorphism groups.
    % This is clear, as \(\Hom(f,f)=\widehat{T}^Q(\Lambda)\) for any \(\Lambda\)-valued cocylce \(f\).
    % This shows that \(\Psi\) gives us isomorphisms 
    % \begin{equation*}
    %     [Z^1(W_E/P,\widehat{T})/\widehat{T}]\cong [\Hom(T(E)/\Theta(P),\gm)/\widehat{T}^Q]
    % \end{equation*}
    % Running over all \(P\subset F^\times\) Galois-stable, pro-\(p\) and open subgroups gives us an open cover of \([Z^1(W_E,\widehat{T})/\widehat{T}]\).
    % Meanwhile on the other side we get \(\Theta(P)\), these form a neighborhood basis by open subgroups of the identity in \(T(E)\) by \cref{lem: condensed H_1 of quotient is torus quotient}.
    
    By abstract nonsense the map \(\Psi_P\) induces an isomorphism 
    \begin{equation*}
        \pi_0[Z^1(W_{F/E}/P,\widehat{T})/_p\widehat{T}](\Lambda)\cong \Hom(H_1(W_{F/E},\widehat{L}),\gm)(\Lambda)
    \end{equation*}
    when \(\Lambda^\times\) is an injective abelian group and the index \(p\) denotes the pre-stack quotient.
    By \cref{lem: units fpqc locally divisible} the fpqc-sheafification of \(\pi_0[Z^1(W_{F/E}/P,\widehat{T})/_p\widehat{T}]\) is \(\Hom(H_1(W_{F/E},\widehat{L}),\gm)\).
    Note that \([Z^1(W_{F/E}/P,\widehat{T})/\widehat{T}]\), which is a priori only the fppf-stackification of the prestack quotient \([Z^1(W_{F/E}/P,\widehat{T})/_p\widehat{T}]\), is actually an fpqc-stack and thus is also the fpqc-stackification.
    This is because fpqc-\(\widehat{T}\)-torsors are already fppf-locally trivial, since \(\widehat{T}\to\spec(\zl)\) is fppf.
    By \cref{lem: applying functor to sheaves} the sheafification of \(\pi_0[Z^1(W_{F/E}/P,\widehat{T})/_p\widehat{T}]\) is isomorphic to the sheafification of \(\pi_0[Z^1(W_{F/E}/P,\widehat{T})/\widehat{T}]\).
    Finally we need to see that \(\Hom(T(E),\gm)=\bigcup_{P\subset F^\times}\Hom(T(E)/\Theta(P),\gm)\), where \(P\subset F^\times\) runs over open subgroups of \(F^\times\) such that \(\Theta(P)\) is open pro-\(p\).
    This follows from \cref{lem: condensed H_1 of quotient is torus quotient}.

    % A more careful analysis will show that this still works if we are allowed to work fpqc-locally.
    % We will follow the arguments of \cite[page 241]{langlandstori}.
    % Fix a open subgroup \(P\subset F^\times\) such that \(\Theta(P)\) is pro-\(p\).
    % Let \(\Psi_P\) the restriction of \(\Psi\) to \([Z^1(W_{F/E}/P,\widehat{T})/\widehat{T}]\).
    % Assume that \(\Psi_P(f)=0\).
    % Then 
    % \begin{equation*}
    %     \sum m_i\otimes [g_i]\mapsto \sum f(g_i)(m_i)
    % \end{equation*}
    % is a well defined map \(\widehat{L}\otimes \bbZ[W_{F/E}/P]\to\Lambda\).
    % It vanishes on those \(\sum m_i\otimes [g_i]\) that are cycles.
    % This means we have a map \(\bar{f}\) on \(B_0\to\Lambda^\times\) where \(B_0\subset\widehat{L}\) is spanned by \(gm-m\) for \(g\in Q\), \(m\in\widehat{L}\), by sending \(gm-m\mapsto f(g)(m)\).
    % After choosing some integer \(N\) we can find some subgroup \(C\subset\frac{1}{N}\widehat{L}\) such that \(\frac{1}{N}B_0\oplus C=\frac{1}{N}\widehat{L}\).
    % Thus we may extend \(\bar{f}\) to a map \(f'\from\widehat{L}\to\Lambda\) after adding \(N\)-th roots to some elements of \(\Lambda\), corresponding to the image of generators of \(B_0\) under \(f\).
    % This can be done fppf-locally.
    % Then \(f-df'\) vanishes on all the 1-chains \(\sum m_i\otimes [g_i]\).
    % Thus fppf-locally there exist an element \(t\in\widehat{T}(\Lambda)\) such that \(t\from f\to 0\) is an isomorphism.
    % It follows that \(f=0\) fppf-locally in \(\pi_0\).
    % Conversely let \(g\from H_1(W_{F/E}/P,\widehat{L})\to\Lambda^\times\).
    % Then this defines a map \(g'\from Z_1(W_{F/E}/P,\widehat{L})\to\Lambda^\times\).
    % After tensoring with \(\bbQ\) the natural inclusion \(Z_1(W_{F/E}/P,\widehat{L}\injto\widehat{L}\otimes \).
\end{proof}