\section{Topology of relatively discrete \texorpdfstring{$\zl$}{Zl}-modules}\label{topology of relatively discrete zl-modules}
In this chapter we want to collect some general properties of $\zl$-modules equipped with their relatively discrete topology.
The main result will be that for our purposes it does not matter whether we consider the relatively discrete topology on a \(\zl\)-module \(M\) or the condensed module \(\cond{M}\otimes_{\zld}\zls\),
where \(\zld\) is \(\zl\) with the discrete condensed structure and \(\zl\) is considered with its natural condensed structure.
When we say \enquote{$\zl$-module} without any qualifiers we  equip the module with the discrete topology.
\begin{defn}
    Let $M$ be a $\zl$-module. We write
    \[
        M_{\ladic}\defined\colim_{N\text{ f.g.}}N_{\ladic}
    \]
    where the colimit is considered in the category of topological spaces. For a finitely generated $\zl$-module $N$ we define $N_{\ladic}$ as $N$ equipped with the $\ell$-adic topology.
    
    More explicitly, a subset $U\subseteq M_{\ladic}$ will be considered open if its intersection with each finitely generated module $N$ is open in the natural $\ell$-adic topology.
\end{defn}
% \begin{lem}
%   The group \(M_{\ladic}\) is a topological group.
% \end{lem}
% \begin{proof}
%   It suffices to show that given an open neighborhood \(U\subset M_{\ladic}\) of \(0\) and \(x\in M\) there is an open neighborhood of \((x,x)\in M_{\ladic}\times M_{\ladic}\) that is contained in the preimage of \(U\) under the map \((x,y)\mapsto x-y\).
%   By translating with \(-x\) it is even sufficient to check this only for \(x=0\).
% \end{proof}
\begin{lem}\label{lem:fg module Hausdorff}
    If $M$ is finitely generated, then $M_{\ladic}$ is compact and Hausdorff.
\end{lem}
\begin{proof}
    Hausdorff follows from Krull's intersection theorem.
    For compactness note that there is a continuous surjection
    \[
        (\zl^{\oplus n})_{\ladic}\surjto M_{\ladic}
    \]
    and $(\zl^{\oplus n})_{\ladic}$ is compact.
\end{proof}
\begin{lem}\label{lem: inclusion to directed colimit}
    Let $(X_i)_{i\in (I,\leq)}$ be a directed system of topological spaces, whose transition maps are closed embeddings. Then the canonical maps $X_j\to\varinjlim X_i$ are closed embeddings. 
\end{lem}
\begin{proof}
    It is clear that $X_j\to\varinjlim X_i$ is injective. We need to check that if $A\subseteq X_j$ is a closed subset in $X_j$, then it is also closed in $\varinjlim X_i$.
    For this we need to check that $A\cap X_i$ is closed in $X_i$ for all $i\in I$.
    This can be checked in an $X_k$ such that $X_i,X_j\subseteq X_k$, since all transition maps are closed embeddings. In this case it is obvious.
\end{proof}
\begin{lem}\label{lem:directed colimit of T1}
    Let $(X_i)_{i\in (I,\leq)}$ be a directed system of $T_1$-spaces, whose transition maps are closed embeddings. Then $X\defined\varinjlim X_i$ is a $T_1$-space.
\end{lem}
\begin{proof}   
    \(T_1\) is equivalent to points being closed. 
    Thus the claim follows immediately from \cref{lem: inclusion to directed colimit}.
    % Let $x,y\in X$. We find an index $i\in I$ such that $x,y\in X_i$. Since $X_i$ is $T_1$, we obtain two open subsets $U_0$ and $V_0$ of $X_i$, such that $x\in U_0$, $y\in V_0$, $x\notin V_0$, $y\notin U_0$.
    % As $X_i\subseteq X$ is a closed embedding by \cref{lem:inclusion to directed colimit}, we find $U$ and $V$ open subsets of $X$ such that $U_0=U\cap X_i$ and $V_0=V\cap X_i$.
    % Then $x\in U$, $y\in V$, $x\notin V$ and $y\notin U$.
\end{proof}
\begin{lem}\label{lem: compact factors through fg}
    Let $A$ be a compact topological space, $M$ a $\zl$-module. Then any continuous map $A\to M_{\ladic}$ factors through a finitely generated submodule.
\end{lem}
\begin{proof}
    This is immediate from \cite[Proposition A.15 (i)]{global_homotopy} applied to the poset of finitely generated submodules of \(M\).
    The conditions are fulfilled as \(\zl\) is noetherian.
    % The image of $A$ will be compact, so we may assume that $A$ is a subset of $M_{\ladic}$.
    
    % Assume that $A$ is not contained in any finitely generated submodule. Then we can find an ascending chain of finitely generated submodules
    % \[
    %   M_0\subsetneq M_1\subsetneq M_2\subsetneq\dots
    % \]
    % such that $A\cap (M_{i+1}\setminus M_{i})\neq\emptyset$.
    
    % We want to see that the natural map $\varinjlim M_{i,\ladic}\to M_{\ladic}$ is a closed embedding.
    
    % It is clear that this map is injective. Let $B\subseteq\varinjlim M_{i,\ladic}$ be a closed subset, so $B\cap M_{i,\ladic}$ is closed for all $i$.
    % We need to check that for all finitely generated submodules $N$ we have $B\cap N$ is a closed subset of $N_{\ladic}$.
    % For this note that
    % \begin{align*}
    %   B\cap N_{\ladic}=B\cap(\varinjlim M_{i,\ladic})\cap N=B\cap(\varinjlim M_{i,\ladic}\cap N_{\ladic})
    % \end{align*}
    % Since $N$ is noetherian, we have $\varinjlim M_i\cap N=M_{j_0}\cap N$ for some index $j_0$.
    % It follows that
    % \begin{align*}
    %   B\cap N_{\ladic}=B\cap M_{j_0,\ladic}\cap N_{\ladic}\subseteq N_{\ladic}
    % \end{align*}
    % Since $M_{j_0,\ladic}\subseteq N_{\ladic}$ is a closed embedding ($M_{j_0,\ladic}$ is compact and $N_{\ladic}$ is Hausdorff), it follows that $A\cap N_{\ladic}$ is closed in $N_{\ladic}$.
    
    % It follows that $A\cap\varinjlim M_i$ is a compact subset of $\varinjlim M_i$, that is not contained in any $M_i$. This is impossible.
    % %todo, but cf https://math.stackexchange.com/questions/1584667/compact-subset-in-colimit-of-spaces
    % %todo include proof of this?, maybe in schwede
\end{proof}
\begin{lem}\label{lem: condensation commutes with colimits of discrete modules}
    Let $R$ be a discrete ring. The functor $\Mod_R\to\condmod{\cond{R}}, M\to\cond{M}$ commutes with small colimits.
\end{lem}
\begin{proof}
    Let us start with filtered colimits.
    
    Since the forgetful functor from $\Mod_R$ to $\mathrm{Set}$ commutes with filtered colimits, we reduce to the statement for discrete sets.
    For this, observe that $\cond{\colim T_i}\cong\colim\cond{T_i}$ for any filtered diagram $(T_i)_{i\in I}$ of discrete sets.
    This is because the image of any continuous map $f\from A\to\colim T_i$ from a compact space will be finite as the target is discrete and the image is compact. Since the indexing category is filtered this means we can find an index $i\in I$ such that $f$ factors through $T_i$.
    
    For finite coproducts note that they agree with products.
    
    Finally, we are left with quotients. Let $M$ be an $R$-module, $N$ a submodule, $S$ a profinite set.
    There is a natural map $\cond{M}(S)/\cond{N}(S)\to\cond{M/N}(S)$ sending a map $S\to M$ to the composite $S\to M\to M/N$.
    It is injective, so we need to check that it is also surjective.
    For this, let us fix a continuous map $f\from S\to M/N$.
    Then write $S$ as a disjoint union of open subsets $U_i$ such that $f|_{U_i}$ is constant.
    Choose lifts of $f|_{U_i}$ to $M$. Since the $U_i$ are disjoint, this glues to a lift of $f$ to a map $S\to M$.
\end{proof}
\begin{lem}\label{lem: mladic is condensed base change}
    Let $M$ be a $\zl$-module. Then $(\cond{M}\otimes_{\zld}\zls)(*)_{\mathrm{top}}=M_{\ladic}$
\end{lem}
\begin{proof}
    Choose some uncountable strong limit cardinal $\kappa$ such that $\cond{M}\otimes_{\zld}\zls$ comes from a $\kappa$-condensed set and such that $M_{\ladic}$ is $\kappa$-compactly generated (for the latter, $\kappa\geq 2^{2^{|M|}}$ suffices)
    
    We note that the functor sending a $\kappa$-condensed set to its underlying topological space commutes with colimits in $\kappa$-compactly generated topological spaces. Let us first assume that $M$ is finitely generated. Writing $M=\coeq(\zld^{\oplus n}\rightrightarrows\zld^{\oplus m})$ we see that
    \[
        \cond{M}\otimes_{\zld}\zls=\coeq(\zls^{\oplus n}\rightrightarrows\zls^{\oplus m})
    \]
    so
    \[
        (\cond{M}\otimes_{\zld}\zls)(*)_{\mathrm{top}}=\coeq((\zls^{\oplus n})_{\ladic}\rightrightarrows(\zls^{\oplus m})_{\ladic})
    \]
    which is precisely $M_{\ladic}$.
    We want to obtain the general case by reducing 
    to the case of finitely generated modules. 
    
    The claim follows as $M_{\ladic}$ is given the colimit topology (which agrees with the one in $\kappa$-compactly generated spaces) with respect to  finitely generated submodules of $M$ and all relevant functors commute with colimits.
\end{proof}
\begin{lem}\label{lem: condensed base change is mladic}
    Let $M$ be a $\zl$-module. Then $\cond{M_{\ladic}}=\cond{M}\otimes_{\zld}\zls$.
\end{lem}
\begin{proof}
    This follows from the equivalence between compact Hausdorff spaces and qcqs condensed sets \cite[Theorem 2.16]{condensed} and \cref{lem: mladic is condensed base change} for finitely generated $\zl$-modules.
    For the general case it is sufficient to see that
    \[
        \cond{\colim N_{\ladic}}\cong\colim\cond{N_{\ladic}}
    \]
    where the colimit is taken over all finitely generated submodules of $N$. This follows from \cref{lem: compact factors through fg}.
\end{proof}
\begin{lem}
    Let \(M\) be a \(\zl\)-module. 
    Then \(M_{\ladic}\) is a group in compactly generated topological spaces.
    If \(M\) is a \(\zl\)-algebra then \(M_{\ladic}\) is a ring in compactly generated topological spaces.
\end{lem}
\begin{proof}
    This is immediate from 
    \begin{align*}
        (M\times M)_{\ladic}&=\cond{(M\times M)}\otimes_{\zld}\zls(*)_{\mathrm{top}}\\
        &=((\cond{M}\otimes_{\zld}\zls)\times(\cond{M}\otimes_{\zld}\zls))(*)_{\mathrm{top}}\\
        &=(M_{\ladic}\times M_{\ladic})^{\mathrm{cg}}
    \end{align*}
    and \(\cond{M}\otimes_{\zld}\zls\) being a condensed group.
\end{proof}
\begin{lem}
    Let $M$ be a $\zl$-module. Then $M_{\ladic}$ is weak Hausdorff.
\end{lem}
\begin{proof}
    By \cite[Proposition A.6]{global_homotopy} it is sufficient to show that it is $T_1$, because then $\{0\}$ will be a closed subset.
    This follows from \cref{lem:fg module Hausdorff} and \cref{lem:directed colimit of T1}.
\end{proof}
\begin{lem}\label{lem: compact-open = natural topology on GL_n}
    Let \(R\) be a locally compact topological ring whose topology is induced by a metric \(d\).
    Then the topology on \(\GL_n(R)\) induced  by the inclusion \(\GL_n(R)\subset\mathrm{Mat}_{n\times n}(R)\) agrees with the subspace topology of \(\Hom(R^n,R^n)\) equipped with the compact-open topology.
\end{lem}
\begin{proof}
    This is essentially \cite[Proposition 1.4.]{nlab:general_linear_group}, let us include a proof for the reader's convenience.
    The assumptions tell us that \(\GL_n(R)\) is locally compact.
    The inclusion \(\GL_n(R)\injto\Hom(R^n,R^n)\) is continuous as it is adjoint to the natural action map \(\GL_n(R)\times R^n\to R^n\).
    This already shows that the natural topology on \(\GL_n(R)\) is equal or finer than the subspace topology coming from the compact-open topology of \(\Hom(R^n,R^n)\).
    Let 
    \begin{equation*}
        U_A^{\varepsilon}\defined \{B\in \mathrm{Mat}_{n\times n}\mid d_{R^n}(Ae_i-Be_i)<\varepsilon, 1\leq i\leq n\}
    \end{equation*}
    where \(e_i\) are the standard basis vectors for \(R^n\) and \(d_{R^n}\) is the product metric induced by \(d\).
    A neighborhood base of \(A\in\GL_n(R)\) consists of \(U_A^{\varepsilon}\cap \GL_n(R)\) for \(\varepsilon>0\).
    This is also a base element for the compact-open topology on \(\Hom(R^n,R^n)\), namely 
    \begin{equation*}
        U_A^{\varepsilon}=\bigcap_{i=1}^n V_i^{K_i}
    \end{equation*}
    where \(K_i=\{e_i\}\) and \(V_i=\{x\in R^n\mid d_{R^n}(x,Ae_i)<\varepsilon\}\) and \(V_i^{K_i}\) denotes the functions \(R^n\to R^n\) that send \(K_i\) to \(V_i\).
\end{proof}
\begin{rem}
    In general the topology on \(\GL_n(R)\) considered above is not the natural topology that one usually considers.
    For \(R=\zl\) they agree.
\end{rem}
\begin{lem}\label{lem: continuous action of locally pro p on ladic}
    Let $G$ be a locally pro-$p$ group, let $M$ be a $\zl$-module.
    Then any continuous linear $G$-action on $M_{\ladic}$ has open stabilizers.
    In particular, any continuous linear $G$-action on $M_{\ladic}$ is in fact continuous even if we equip $M$ with the discrete topology.
\end{lem}
\begin{proof}
    It is sufficient to show this for a compact-open subgroup of $G$, so we may assume that $G$ is pro-$p$.
    Let $m\in M$ be an element.
    It follows by \cref{lem: compact factors through fg} that the orbit $G.m$ is contained in a finitely generated submodule.
    In particular, the submodule generated by $G.m$ will be finitely generated and $G$-stable.
    Thus we may assume that $M$ is finitely generated (in particular compact and Hausdorff).
    A $G$-action on $M$ is the same as a continuous map of groups $G\to\aut(M)$ where we equip $\aut(M)$ with the compact-open topology.
    Since $M$ is finitely generated and $\zl$ is a principal ideal domain, we can write $M$ as 
    \[
        \zl^{\oplus n}\oplus\bigoplus_{i\geq 0}^n\bbZ/\ell^{n_i}\bbZ
    \] 
    Any element in $\aut(M)$ will be of the form
    \[
        \begin{pmatrix}
            a&0\\
            b&d
        \end{pmatrix}
    \]
    where $a\in\GL_n(\zl)$, $d\in\aut(\bigoplus_{i\geq 0}^n\bbZ/\ell^{n_i}\bbZ)$ and $c\in\Hom(\zl^{\oplus n},\bigoplus_{i\geq 0}^n\bbZ/\ell^{n_i}\bbZ)$.
    It follows that $\aut(M)$ equipped with the compact-open topology is locally pro-$\ell$, using \cref{lem: compact-open = natural topology on GL_n}.
    Thus, the map $G\to\aut(M)$ has an open kernel.
\end{proof}
We can also give a proof using condensed mathematics taking advantage of the fact that compact Hausdorff spaces give rise to compact condensed sets.
\begin{proof}
    It suffices to show this for a compact-open subgroup of \(G\), so we may assume that \(G\) is pro-\(p\).
    Since \(G\) is locally compact, the product \(G\times M_{\ladic}\) in compactly generated spaces can be computed in topological spaces.
    It follows that continuous actions \(G\times M_{\ladic}\to M_{\ladic}\) are equivalent to condensed \(\cond{G}\) actions on \(\cond{M}\otimes_{\zld}\zls\).
    Given \(m\in M\) we get a map of condensed sets \(a_m\from\cond{G}\to \cond{M}\otimes_{\zld}\zls\) by acting on \(m\).
    Note that \(\cond{G}\) is a quasi-compact condensed set and \(\cond{M}\otimes_{\zld}\zls\) is the filtered colimit of \(\cond{N}\otimes_{\zld}\zls\) where \(N\) runs through the finitely generated submodules of \(M\) by \cref{lem: condensation commutes with colimits of discrete modules}.
    Thus the map \(a_m\) factors through a finitely generated submodule with its relatively discrete condensed structure.
    Therefore we may replace \(M\) with the submodule generated by the image of \(a_m\), which is \(G\)-stable.
    We can now conclude as in the previous proof.
\end{proof}
% \begin{lem}
%   Let \(R\) be a topological ring.
%   Then we have an isomorphism of condensed groups \(\cond{\GL_n(R)}\cong\GL_n(\cond{R})\).
% \end{lem}
% \begin{proof}
%   \(\GL_n(\cond{R})\) is the fiber of the map \(\End(\cond{R}^n)\times\End(\cond{R}^n)\to \End(\cond{R}^n)\) given by composition over the identity.
%   This map is also the morphism of condensed sets induced by the continuous map \(\End(R^n)\times \End(R^n)\to \End(R^n)\) given by composition, where everything carries the topology induced from \(R\).
%   Taking fibers commutes the functor from T1 topological spaces to condensed sets, which shows the claim.
% \end{proof}
% \begin{cor}
%   Let \(R\) be a topological ring.
%   Let \(G\) be a linear algebraic group over \(R\).
%   Then \(\cond{G(R)}\cong G(\cond{R})\).
% \end{cor}
% \begin{proof}
%   By assumption we can find a morphism of \(R\)-schemes \(\bbA^n_R\to\bbA^m_R\) such that \(G\) is the fiber over \(0\).
%   Thus \(G(\cond{R})\) is the fiber over \(0\) of the induced map of condensed sets \(\cond{R}^n\to\cond{R}^m\).
%   This comes from a continuous map of topological spaces \(R^n\to R^m\).
% \end{proof}
 
 

