\section{Categorical Computations}
\begin{lem}\label{lem: QCoh(Z^1(W Gm)) is WMod}
    Let \(W\) be an abelian locally pro-\(p\) group.
    Then \(\qcoh(\Hom(W,\gm))\) is equivalent to the category of smooth \(W\)-representations on \(\zl\)-modules. (By \cref{lem: continuous action of locally pro p on ladic} it does not matter if we equip \(\zl\) with the discrete or natural topology)
\end{lem}
\begin{proof}
    Let \(P\subset P'\) be compact open pro-\(p\) subgroups of \(W\), \(f\from W/P\mhyphen\Mod_{\zl}\to W\mhyphen\Mod_{\zl}\) the functor induced by \(W\to W/P\) and \(f'\) similarly but using \(P'\) instead of \(P\).
    Let \(j_P\from Z^1(W/P,\gm)\to Z^1(W,\gm)\) and \(i\from Z^1(W/P',\gm)\to Z^1(W/P,\gm)\) be the inclusions.
    We get a functor \(\qcoh(Z^1(W,\gm))\to W\mhyphen\Mod_{\zl}\) via \(\mathcal{F}\mapsto\colim_{P'\supset P} f(\Gamma(j_P^*\mathcal{F}))\).
    The transition maps are induced from 
    \begin{equation*}
        i_!j_{P'}^*\mathcal{F}=i_!j_{P'}^*j_P^*\mathcal{F}\to j_P^*\mathcal{F}
    \end{equation*}
    noting that \(\Gamma(i_!j^*_{P'}\mathcal{F})=\Gamma(j_{P'}^*\mathcal{F})\), as \(i\) is the inclusion of an open and closed subscheme.
    It is clear that this will yield a smooth representation, as each \(f(\Gamma(j_P^*\mathcal{F}))\) is.
    We also obtain a functor \(W\mhyphen\Mod_{\zl}\to\qcoh(Z^1(W,\gm))\) by mapping \(M\mapsto\colim_{P'\supset P}j_{P'!}\widetilde{M_{P'}}\).
    Note that 
    \begin{equation*}
        j_{P'!}\widetilde{M_{P'}}\subset j_{P!}\widetilde{M_P}
    \end{equation*}
    To see this, observe that \(i^*\widetilde{M_{P}}=\widetilde{M_P}\).
    Thus we get a map
    \begin{equation*}
        i_!\widetilde{M_{P'}}=i_!i^*\widetilde{M_P}\to \widetilde{M_P}
    \end{equation*}
    Now apply \(j_{P!}\) and note that \(j_{P!}i_!i^*=j_{P'!}i^*\).
    One checks that it is an inclusion on stalks.
    For the second functor, note that \(j_{P'}\) is the inclusion of an open and closed subscheme, so it is quasi-coherent.
    Now let us check that the functors are inverse to each other.
    We claim that the map \(\Gamma(j_{P'}^*\mathcal{F})\to\Gamma(j_P^*\mathcal{F})\) is given by 
    \begin{equation*}
        \Gamma(j_{P'}^*)\cong\Gamma(j_P^*\mathcal{F})_{P'/P}\cong\Gamma(j_P^*\mathcal{F})^{P'/P}\subset\Gamma(j_P^*\mathcal{F})
    \end{equation*}
    Let \(t\in\zl[W/P]\) be defined as 
    \begin{equation*}
        t\defined\frac{1}{|P'/P|}\sum_{[g]\in P'/P}g
    \end{equation*}
    Observe that \(t\) is idempotent and that \(D(t)\subset Z^1(W/P,\gm)\) is precisely the morphism \(Z^1(W/P',\gm)\to Z^1(W/P,\gm)\).
    The map \(\Gamma(j_{P'}^*\mathcal{F})\to\Gamma(j_P^*\mathcal{F})\) takes an \(s\in\Gamma(j_{P'}^*\mathcal{F})\) and assigns it to the element \(s'\in\Gamma(j_P^*\mathcal{F})\) satisfying \(s'|_{D(t)}=s\) and \(s'|_{D(1-t)}=0\).
    One checks that this shows that the map \(\Gamma(j_{P'}^*\mathcal{F})\to\Gamma(j_P^*\mathcal{F})\) has the form we claimed.
    One way the composite of the two functors sends an \(M\in W\mhyphen\Mod_{\zl}\) to 
    \begin{align*}
        \colim_{P}f(\Gamma(j_P^*(\colim_{P'}j_{P'!}\widetilde{M_{P'}})))&=\colim_{P}f(\Gamma(\colim_{P'}j_P^*(j_{P'!}\widetilde{M_{P'}})))\\
        &=\colim_{P}f(\Gamma(\widetilde{M_P}))\\
        &=\colim_P M_P
    \end{align*}
    Thus we need to show that \(M\cong\colim_P M_P\) naturally in \(M\).
    For this we construct a map \(\colim M_P\to M\).
    Consider an element \([(s,P')]\in\colim M_P\) such that \(s\in M_{P'}\).
    It is identified with \([(s,P)]\) for \(P\subset P'\) via the inclusion 
    \begin{equation*}
        M_{P'}=(M_P)_{P'/P}\cong M_P^{P'/P}\subset M_P
    \end{equation*}
    Taking \(P\) small enough, since \(M\) is a smooth representation, we find a unique lift \(s\in M\).
    It is clear that this map is bijective and the construction is natural in \(M\)
    For the other direction, a sheaf \(\mathcal{F}\) is sent to 
    \begin{equation*}
        \colim_P j_{P!}(\colim_{P'}f'(\Gamma(j_{P'}^*\mathcal{F}))_{P})^{\sim}
    \end{equation*}
    Let us construct isomorphism between \(j_U^*\mathcal{F}\) and \(j_U^*\) applied to this term where \(U\) is some compact open pro-\(p\) subgroup of \(W\).
    \begin{align*}
		j_U^*\colim_{P'} j_{P'!}((\colim_Pf\Gamma(j_P^*\mathcal{F}))_{P'})^\sim&=\colim_{U\supset P'} j_U^*((\colim_Pf\Gamma(j_P^*\mathcal{F}))_{P'})^\sim\\
		&=\colim_{U\supset P'} j_U^*(\colim_{P'\supset P}(\Gamma(j_P^*\mathcal{F})_{P'/P})^\sim\\
		&=\colim_{U\supset P'} j_U^*((\Gamma(j_{P'}^*\mathcal{F}))^\sim\\
		&=j_U^*\mathcal{F}
	\end{align*}
	These isomorphisms glue and are natural in $\mathcal{F}$.
\end{proof}
\begin{cor}\label{cor: subcategory given by e_P}
    We consider the setting of \cref{lem: QCoh(Z^1(W Gm)) is WMod}.
    Let \(P\subset W\) be an open pro-\(p\) subgroup.
    By \cref{lem: moduli of condensed cocycles representable}, we obtain an idempotent \(e_P\in\oo(\Hom(W,\gm))\), corresponding to the inclusion \(j\from\Hom(W/P,\gm)\injto\Hom(W,\gm)\).
    The subcategory of \(\D(\qcoh(\Hom(W,\gm)))\simeq\D(W\mhyphen\Mod_{\zl})\) spanned by those objects where \(e_P\) acts by the identity is given by \(\D(W/P\mhyphen\Mod_{\zl})\).
\end{cor}
\begin{proof}
    The natural transformation \(e_P\) maybe be described as \(\id\to j_*j^*\cong j_!j^*\to\id\) geometrically, by the proof of \cref{lem: QCoh(Z^1(W Gm)) is WMod}.
    In geometry it is clear that the statement holds.
\end{proof}
\begin{prop}\label{prop: spectral category is automorphic category}
    For a \(\zl\)-algebra \(A\) there is an equivalence of categories
    \begin{equation*}
        \qcoh(\spec(A)/\widehat{T}^Q)\simeq\prod_{b\in B(T)}\qcoh(\spec(A))
    \end{equation*}
    whenever \(\widehat{T}^Q\) acts trivially on \(\spec(A)\).
\end{prop}
\begin{proof}
    We need to determine the \(\widehat{T}^Q\)-equivariant sheaves on \(\spec(A)\).
    First observe that \(\widehat{T}^Q=\spec(\zl[\underline{x}]/(\underline{x}^\mu-\underline{x}^{q.\mu},\mu\in X_*(T),q\in Q))\).
    Here \(\underline{x}^\mu=x_1^{\mu_1}\cdots x_n^{\mu_n}\) whenever \(\mu=(\mu_1,\dots,\mu_n)\).
    By the same proof as in \cite[Tag 0EKK]{stacks} we see that 
    \begin{equation*}
        \qcoh([\spec(A)/\widehat{T}^Q])=\prod_{X_*(T)_Q}\qcoh(\spec(A))
    \end{equation*}
    whenever \(\widehat{T}^Q\) acts trivially on \(\spec(A)\)(as we have the relation \(\underline{x}^\mu-\underline{x}^{q.\mu}\) we only get the degree in \(X_*(T)_Q\)).
\end{proof}
\begin{constr}
    Let $X$ be an algebraic stack.
	Then $\qcoh(X)=\heart{\dqc(X)_{\geq 0}}$, so by \cite[Theorem C.5.4.9]{sag} we obtain a functor $\Psi_{X,\geq 0}\from \D(\qcoh(X))_{\geq 0}\to\dqc(X)_{\geq 0}$, that is a left adjoint and left exact.
	Passing to stabilizations, we obtain a functor between stable \(\infty\)-categories
	\[
		\Psi_X\from \D(\qcoh(X))\to\dqc(X)
	\]
	natural in $X$, that is a left adjoint.
\end{constr}
\begin{lem}\label{lem: D^+(QCoh)=D^+_QCoh}
    Let \(\mathcal{X}\) be the disjoint union of quasi-compact stacks with affine diagonal.
    The functor \(\Psi\from \D(\qcoh(\mathcal{X})\to \dqc(\mathcal{X})\) restricts to an equivalence of stable \(\infty\)-categories 
    \begin{equation*}
        \D^+(\qcoh(\mathcal{X}))\simeq \D^+_{\qcoh}(\mathcal{X})
    \end{equation*}
\end{lem}
\begin{proof}
    Write \(\mathcal{X}=\bigsqcup_{i\in I}\mathcal{X}_i\) as a disjoint union of quasi-compact stacks with affine diagonal.
    For each of the \(\mathcal{X}_i\) we have 
    \begin{equation*}
        \Psi_{\mathcal{X}_i}\from \D^+(\qcoh(\mathcal{X}_i))\simeq \D^+_{\qcoh}(\mathcal{X}_i)
    \end{equation*}
    This is \cite[Theorem C.1.]{hnr19}.
    Note that \(\D(\qcoh(-))\) and \(\dqc(-)\) send disjoint unions of stacks to products of \(\infty\)-categories, for the former this is \cref{cor: product of derived categories is derived category of products}, for the latter it is fpqc descent.
    We can thus identify \(\D^+(\qcoh(\mathcal{X}))\subset \prod_{i\in I}\D(\qcoh(\mathcal{X}_i))\) as those complexes that are globally bounded below for \(i\in I\).
    A similar story holds for \(\D^+_{\qcoh}(\mathcal{X})\subset\prod_{i\in I}\D^+_{\qcoh}(\mathcal{X}_i)\).
    Observe that \(\Psi_{\bigsqcup X_i}=\prod_{i\in I}\Psi_{X_i}\) by naturality of \(\Psi\).
    Thus we can deduce the lemma from the case of the \(\mathcal{X}_i\).
\end{proof}
% \begin{lem}
% 	There is an equivalence of \(\infty\)-categories
% 	\[
% 		D^b(\coh([Z^1(W_E,\widehat{T})/\widehat{T}]))\simeq D^b_{\coh}([Z^1(W_E,\widehat{T})/\widehat{T}])
% 	\]
%     induced by the natural functor \(\Psi\from D(\qcoh(\locsys_{\widehat{T}}))\to\dqc(\locsys_{\widehat{T}})\) of \(\infty\)-categories.
%     Here \(D^b(\coh([Z^1(W_E,\widehat{T})/\widehat{T}]))\) is the full stable sub-\(\infty\)-category of \(D^b(\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}]))\) that can be represented by a complex of coherent sheaves.
%     This is identified with \(D^b_{\coh}(\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}]))\) by induction, since coherent sheaves form a Serre subcategory.
%     (This shows in particular that it is a stable sub-\(\infty\)-category)
% \end{lem}
% \begin{proof}
%     We can write \([Z^1(W_E,\widehat{T})/\widehat{T}]=\bigsqcup_{i\in I}X_i\) as a disjoint union of noetherian stacks.
%     Both \(D(\qcoh(-))\) and \(\dqc(-)\) turn coproducts on stacks to products of categories (use \cref{cor: product of derived categories is derived category of products} for the first one).
%     Thus \(D^b_{\coh}([Z^1(W_E,\widehat{T})/\widehat{T}])\subset\prod_{i\in I}D^b_{\coh}(X_i)\) spanned by those complexes whose cohomology is globally bounded in the \(i\in I\).
%     We have a similar description for \(D^b(\coh([Z^1(W_E,\widehat{T})/\widehat{T}]))\subset \prod_{i\in I}D^b(\coh([Z^1(W_E,\widehat{T})/\widehat{T}]))\).

%     Thus we reduce to the claim that \(D^b(\coh(X))\simeq D^b_{\coh}(X)\) whenver \(X\) is a noetherian stack.
%     Note that this is true when we replace ``\(\coh\)'' by ``\(\qcoh\)'', see the towards the end of the proof of \cite[Theorem C.1.]{hnr19} (they only prove this on the homotopy category, but an exact functor of stable \(\infty\)-categories is an equivalence if and only if it is an equivalence on the homotopy category).
%     It follows that we have \(D^b_{\coh}(\qcoh(X))\simeq D^b_{\coh}(X)\), but \(D^b_{\coh}(\qcoh(X))=D^b(\coh(X))\) by our observation in the statement of the lemma.
% 	% We claim that for $X\defined [Y/\widehat{T}^Q]$ it becomes an equivalence of stable $\infty$-categories, when $Y$ is an affine scheme.
% 	% For this it is sufficient to check on homotopy categories, as $\Psi_X$ is already a left adjoint.
% 	% This follows from \cite[Theorem 1.2.]{hnr19} and \cite[Example 8.6.]{hr17} (note that $\aleph_0$-crisp in particular means compactly generated).
%     % Note that here we need that \(\ell\) is very good for \(T\): this implies that \(\pi_0(\widehat{T}^Q_{\overline{\bbF_{\ell}}})\) is coprime to \(\ell\), which is \cite[Theorem VIII.5.14.]{geometrization}.
%     % The connected components of \(\widehat{T}^Q\) are tori, it follows by Nagata's characterization of linear reductive groups that \(\widehat{T}^Q_{\overline{\bbF_\ell}}\) is linear reductive.
% 	% Thus, writing
% 	% \[
% 	% 	\locsys_{\widehat{T}}=\bigsqcup_{i\in I} [Y_i/\widehat{T}^Q]
% 	% \]
% 	% as a disjoint union of open substacks with $Y_i$ affine satisfying \(\bigsqcup Y_i=\Hom(T(E),\gm)\), we see
% 	% \begin{align*}
% 	% 	\prod_{b\in B(T)}D(\qcoh(\Hom(T(E),\gm)))
%     %     &\simeq D(\prod_{b\in B(T)}\prod \qcoh(Y_i))\\
%     %     &\simeq D(\prod\qcoh([Y_i/\widehat{T}^Q]))\\
% 	% 	&\simeq\prod D(\qcoh([Y_i/\widehat{T}^Q]))\\
% 	% 	&\simeq\prod\dqc([Y_i/\widehat{T}^Q])\\
% 	% 	&\simeq\dqc(\locsys_{\widehat{T}})
% 	% \end{align*}
% 	% where we repeatedly use \cref{cor: product of derived categories is derived category of products}.
% 	% We now apply \cref{lem: QCoh(Z^1(W Gm)) is WMod}.
% \end{proof}
% \begin{prop}\label{prop: categorical form of fargues' conjecture without spectral action}
% 	We have an equivalence of stable $\infty$-categories
% 	\[
% 		\D^{b,\mathrm{qc}}_{\coh,\nilp}(\locsys_{\widehat{T}})\simeq \D_{\lis}(\bun{T},\zl)^{\omega}
% 	\]
% 	that maps the structure sheaf on \(\locsys_{\widehat{T}}\) to the (unique choice of) Whittaker sheaf.
% \end{prop}
% \begin{proof}
%     Let \(U\) be an open subgroup of \(T(E)\).
    
%     We know that \(\D^{b,\mathrm{qc}}_{\coh,\nilp}(\locsys_{\widehat{T}})=\perf^{\mathrm{qc}}(\locsys_{\widehat{T}})\), this is \cite[Theorem VIII.2.9.]{geometrization}
% 	Under the equivalence $\D^+_{\qcoh}(\locsys_{\widehat{T}})\simeq\prod_{b\in B(T)}\D^+(T(E)\mhyphen\Mod_{\zl})$, a complex \(A\) with quasi-compact support corresponds to those collections complexes of representations $(M_i,d_i)_{b\in B(T)}$ such that there is an open pro-\(p\) subgroup \(U\subset T(E)\) that acts trivially on every $M_i$.
%     Additionally, it must vanish on almost all factors.
% 	This is because pulling back to $\Hom(T(E),\gm)$ corresponds to taking direct sums of all the individual factors and pulling back preserves perfectness (which implies coherent).
%     Note that \([\Hom(T(E)/U,\gm)/\widehat{T}^Q]\) is a qcqs algebraic stack, such that 
%     \begin{equation*}
%         \D^+_{\qcoh}([\Hom(T(E)/U,\gm)/\widehat{T}^Q])=\prod_{b\in B(T)}\D^+(T(E)/U\mhyphen\Mod_{\zl})
%     \end{equation*}
%     and the structure sheaf is just \(\zl[T(E)/U]\) (and each factor is actually just the module category over this ring).
%     Note that a complex \(A\) on \([\Hom(T(E)/U,\gm)/\widehat{T}^Q]\) is perfect if and only if its pullback to \(\Hom(T(E)/U,\gm)\) is.
%     This means when writing \(A=\bigoplus_{b\in B(T)}A_b\) that each \(A\) is a perfect complex of \(\zl[T(E)/U]\)-modules, so it lies in the thick triangulated subcategory generated by \(\zl[T(E)/U]=\cind{U}{T(E)}1\).
%     It follows that the same is true for each of the \(A_b\) as they are just retracts, so each \(A_b\) is compact in \(\D(T(E)\mhyphen\Mod_{\zl})\) and summarizing we have \(A\in \D_\lis(\bun{T},\zl)^\omega\).
% 	Clearly we hit all $\cind{U}{T(E)}1$ as extensions by zero of the structure sheaves on certain open substacks, so the functor hits all compact objects, as \(\perf^{\mathrm{qc}}(\locsys_{\widehat{T}})\) is a thick stable subcategeory of \(\dqc(\locsys_{\widehat{T}})\).
% \end{proof}


\begin{defn}
    Given a character \(\chi\in X^*(\widehat{T})\) we define \(\oo[\chi]\in\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])\) to be \(\oo[\chi]=f^*V_{\chi}\) where \(f\from[Z^1(W_E,\widehat{T})/\widehat{T}]\to B\widehat{T}\) is the natural map and \(V_\chi\in\mathrm{Vect}(B\widehat{T})\) is vector bundle corresponding to the \(\widehat{T}\)-representation attached to \(\chi\in X^*(\widehat{T})\).
\end{defn}
\begin{lem}\label{lem: qcoh decomposition}
    There is a canonical decomposition 
    \begin{equation*}
        \qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])\simeq\prod_{[\chi]\in X^*(\widehat{T}^Q)}\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}
    \end{equation*}
    and we have equivalences 
    \begin{equation*}
        \qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}\simeq\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])_{0}
    \end{equation*}
    given by \(-\otimes\oo[-\chi]\).
    Here \(\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}\) for \([\chi]\in X^*(\widehat{T}^Q)\) is the full subcategory of \(\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])\) spanned by those sheaves \(V\) where the action of the inertia stack is given by \([\chi]\).
\end{lem}
\begin{proof}
    Using \cite[Tag 06PB]{stacks} we can easily see that the inertia stack for \([Z^1(W_E,\widehat{T})/\widehat{T}]\) is given by \(\widehat{T}^Q\times [Z^1(W_E,\widehat{T})/\widehat{T}]\to [Z^1(W_E,\widehat{T})/\widehat{T}]\), where the map is just the projection.
    Thus every quasi-coherent sheaf on \(\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])\) carries an action of \(\widehat{T}^Q\).
    Let us write a quasi-coherent sheaf \(V\in\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])\) in terms of its presentation 
    \begin{align*}
        V_j\otimes_{A_j,\rho}A_j\otimes\oo(\widehat{T})\to V_j\otimes_{A_j,\id}A_j\otimes\oo(\widehat{T})
    \end{align*}
    given by \(v\otimes 1\otimes 1\mapsto\sum v_i\otimes 1\otimes t^i\), where \(Z^1(W_E,\widehat{T})=\bigsqcup_{i\in I}\spec(A_i)\) and \(V_i=V|_{\spec(A_i)}\).
    Then the coaction by \(\oo(\widehat{T}^Q)\) is given by 
    \begin{equation*}
        \begin{tikzcd}[column sep =small]
            v\otimes 1\otimes 1 \arrow[ddd, maps to] \arrow[rrr, maps to] &[-55pt]                                                                                      &                                                                          &[-55pt] \sum v_i\otimes 1\otimes t^i \arrow[ddd, maps to] \\[-10pt]
                                                                          & {V_j\otimes_{A_j,\rho}A_j\otimes\oo(\widehat{T})} \arrow[r] \arrow[d]                & {V_j\otimes_{A_j,\id}A_j\otimes\oo(\widehat{T})} \arrow[d]               &                                                   \\
                                                                          & {V_j\otimes_{A_j,\rho}A_j\otimes\oo(\widehat{T})\otimes\oo(\widehat{T}^Q)} \arrow[r] & {V_j\otimes_{A_j,\id}A_j\otimes\oo(\widehat{T})\otimes\oo(\widehat{T}^Q)} &                                                   \\[-10pt]
            \sum v_i\otimes 1\otimes 1\otimes t^i \arrow[rrr, maps to]    &                                                                                      &                                                                          & \sum v_i\otimes 1\otimes t^i\otimes t^i          
            \end{tikzcd}
    \end{equation*}
    The rest of the lemma follows from our explicit description of the \(\widehat{T}^Q\)-action on \(V\).
\end{proof}
\begin{cor}\label{cor: D^+_QCoh decomposition}
    We can identify \(\D^+_{\qcoh}([Z^1(W_E,\widehat{T})/\widehat{T}]))\) with the full subcategory of \(\prod_{[\chi]\in X^*(\widehat{T}^Q)}\D(\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]})\) spanned by those \((A_{[\chi]})_{[\chi]\in X^*(\widehat{T}^Q)}\) such that there is some \(n\in\bbZ\) such that \(H^n(A_{[\chi]})=0\) for all \([\chi]\in X^*(\widehat{T}^Q)\).
\end{cor}
\begin{proof}
    This follows immediately from \cref{cor: product of derived categories is derived category of products} and \cref{lem: D^+(QCoh)=D^+_QCoh}.
\end{proof}
\begin{lem}\label{lem: max lieblich phd lemma}
    Let \(\pi\from\mathcal{X}\to X\) be a gerbe where \(X\) is an algebraic space.
    Let \(\qcoh(\mathcal{X})_0\) denote the full subcategory of \(\qcoh(\mathcal{X})\) spanned by those objects whose action of the inertia stack \(\mathcal{I}_{\mathcal{X}/X}\) is trivial.
    Then we have an equivalence of categories 
    \begin{equation*}
        \pi_*\from\qcoh(\mathcal{X})_0\simeq\qcoh(X)
    \end{equation*}
\end{lem}
\begin{proof}
    This is basically \cite[Lemma 2.1.1.17]{max_lieblich_phd}, which already covers fully faithfulness.
    Note that since \(\pi\) is qcqs we can compute \(\pi_*\) locally.
    For essentially surjective, we need to check that \(F\to \pi_*\pi^*F\) is an equivalence.
    This can be checked locally, so without loss of generality we are working with a split gerbe over an affine scheme. 
    Then \(\pi_*\) identifies with the subsheaf of sections with trivial action, but for \(\pi^*F\) this is the entire sheaf.
\end{proof}
\begin{cor}\label{cor: derived max lieblich phd lemma}
    Let \(\pi\from\mathcal{X}\to X\) be a gerbe where \(X\) is a an algebraic space.
    Assume that we have equivalences
    \begin{equation*}
        \D^+(\qcoh(Y))\simeq\D_{\qcoh}^+(Y)
    \end{equation*}
    for \(Y=X\) and \(Y=\mathcal{X}\) via the natural functor and that \(\pi_*\) is exact.
    Then we have an equivalence of stable \(\infty\)-categories 
    \begin{equation*}
        \pi_*\from\D_{\qcoh}^+(\mathcal{X})_0\to\D_{\qcoh}^+(X)
    \end{equation*}
    where \(\D_{\qcoh}^+(\mathcal{X})_0\) is the full subcategory spanned by those objects whose action by the inertia stack \(\mathcal{I}_{\mathcal{X}/X}\) is trivial.
    Additionally we have 
    \begin{equation*}
        \D^+(\qcoh(\mathcal{X})_0)\simeq\D_{\qcoh}^+(\mathcal{X})_0
    \end{equation*}
    via the natural functor.
\end{cor}
\begin{proof}
    Observe that the inclusion \(\qcoh(\mathcal{X})_0\injto\qcoh(\mathcal{X})\) admits an exact right adjoint, given by \(\pi^*\pi_*\).
    To see this that it is right adjoint, take \(\pi^*A\in\qcoh(\mathcal{X})_0\), by \cref{lem: max lieblich phd lemma} all objects in \(\qcoh(\mathcal{X})_0\) look like this.
    Then for any \(B\in\qcoh(\mathcal{X})\) we have 
    \begin{equation*}
        \Hom_{\mathcal{X}}(\pi^*A,\pi^*\pi_*B)\cong\Hom_{X}(A,\pi_*B)\cong\Hom_{\mathcal{X}}(\pi^*A,B)
    \end{equation*}
    By assumption \(\pi_*\) is exact and \(\pi^*\) is exact since the inclusion \(\qcoh(\mathcal{X})_0\injto\qcoh(\mathcal{X})\) is.
    Thus by \cref{lem: Bousfield localization preserved by taking derived categories} we see that \(\D^+(\qcoh(\mathcal{X})_0)\) is a full subcategory of \(\D^+(\qcoh(\mathcal{X}))\).
    By \cref{lem: max lieblich phd lemma} we have 
    \begin{equation*}
        \D^+(\qcoh(\mathcal{X})_0)\simeq\D^+(\qcoh(X))\simeq\D_{\qcoh}^+(X)
    \end{equation*}
    This factors over \(\D_{\qcoh}^+(\mathcal{X})_0\), which also shows that 
    \begin{equation*}
        \D^+(\qcoh(\mathcal{X})_0)\simeq\D_{\qcoh}^+(\mathcal{X})_0
    \end{equation*}
\end{proof}
\begin{cor}
    The natural morphism \(\pi\from[Z^1(W_E,\widehat{T})/\widehat{T}]\to Z^1(W_E,\widehat{T})/\!\!/\widehat{T}\) induces an equivalence of categories
    \begin{equation*}
        \pi_*\from \qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])_0\simeq\qcoh(Z^1(W_E,\widehat{T})/\!\!/\widehat{T})
    \end{equation*}
\end{cor}
\begin{cor}
    We have an equivalence of stable \(\infty\)-categories
    \begin{equation*}
        \pi_*\from\D_{\qcoh}^+([Z^1(W_E,\widehat{T})/\widehat{T}])_0\simeq \D_{\qcoh}^+(Z^1(W_E,\widehat{T})/\!\!/\widehat{T})
    \end{equation*}
\end{cor}
\begin{proof}
    By \cref{cor: derived max lieblich phd lemma} we only need to check 
    \begin{equation*}
        \D^+(\qcoh(Y)\simeq\D_{\qcoh}^+(Y)
    \end{equation*}
    for \(Y=[Z^1(W_E,\widehat{T})/\widehat{T}]\) and \(Y=Z^1(W_E,\widehat{T})/\!\!/\widehat{T}\) and that \(\pi_*\) is exact.
    This follows from \cref{lem: D^+(QCoh)=D^+_QCoh} and \cref{lem: qcoh decomposition}.
\end{proof}
\begin{lem}
    There is a canonical decomposition 
    \begin{equation*}
        \perf([Z^1(W_E,\widehat{T})/\widehat{T}])\simeq\bigoplus_{[\chi]\in X^*(\widehat{T}^Q)}\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}
    \end{equation*}
    and we have equivalences 
    \begin{equation*}
        \perf([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}\simeq\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_{0}
    \end{equation*}
    given by \(-\otimes\oo[-\chi]\).
\end{lem}
\begin{proof}
    Given \(A\in \perf([Z^1(W_E,\widehat{T})/\widehat{T}])\) we can write it as \(\prod_{[\chi]\in X^*(\widehat{T}^Q)}A_{[\chi]}\) by \cref{cor: D^+_QCoh decomposition}.
    Each \(A_{[\chi]}\) is a retract of \(A\) and hence lies in \(\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}\).
    The functor \(-\otimes\oo[-\chi]\from\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_{[\chi]}\to\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_0\) is clearly an equivalence with inverse \(-\otimes\oo[\chi]\).
    Finally, to show that it is a direct sum decomposition we need to show that almost all \(A_{[\chi]}\) are trivial.
    Let \(p\from Z^1(W_E,\widehat{T})\to[Z^1(W_E,\widehat{T})/\widehat{T}]\) be the canonical projection.
    One easily computes that \(p^*\prod_{[\chi]\in X^*(\widehat{T}^Q)}A_{[\chi]}=\prod_{[\chi]\in X^*(\widehat{T}^Q)}p^*A_{[\chi]}\) by construction of the decomposition.
    Perfect complexes get pulled back to perfect complexes by \(p\), but this is only possible if almost all \(A_{[\chi]}\) are trivial.
\end{proof}
\begin{lem}
    Let \(\pi\from\mathcal{X}\to X\) be a gerbe where \(X\) is a an algebraic space.
    Assume that we have equivalences
    \begin{equation*}
        \D^+(\qcoh(Y))\simeq\D_{\qcoh}^+(Y)
    \end{equation*}
    for \(Y=X\) and \(Y=\mathcal{X}\) via the natural functor and that \(\pi_*\) is exact.
    Let \(\perf(\mathcal{X})_0\) denote the full subcategory of perfect complexes on \(\mathcal{X}\) where the action of the inertia stack \(\mathcal{I}_{\mathcal{X}/X}\) is trivial.
    Then we have an equivalence of stable \(\infty\)-categories 
    \begin{equation*}
        \pi_*\from\perf(\mathcal{X})_{0}\simeq\perf(X)
    \end{equation*}
\end{lem}
\begin{proof}
    We only need to check that \(\pi_*\) and \(\pi^*\) preserve the respective subcategories in \cref{lem: max lieblich phd lemma}.
    For \(\pi^*\) this is clear.
    For \(\pi_*\), note that \(\pi\) is qcqs, so we have base change.
    Thus we may assume that \(\pi\) has a section \(s\).
    In this case one computes that \(\pi_*\) restricted to \(\perf(\mathcal{X})_0\) identifies with \(s^*\), which naturally preserves perfect complexes.
    Namely there is a natural transformation \(\pi_*\to s^*\) coming from the underived functors (where it is just an inclusion) and the subcategory spanned by those objects where it is an isomorphism is stable and contains \(\perf(\mathcal{X})_0\cap\heart{\perf(\mathcal{X})}\), which generates \(\perf(\mathcal{X})_0\).
\end{proof}
\begin{cor}
    We have an equivalence of stable \(\infty\)-categories
    \begin{equation*}
        \pi_*\from\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_0\simeq \perf(Z^1(W_E,\widehat{T})/\!\!/\widehat{T})
    \end{equation*}
\end{cor}
\begin{lem}\label{lem: degree 0 piece thick stable subcategory of structure sheaf}
    \(\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_0\) is spanned by those objects such that the restriction to any connected component lies in the thick stable subcategory spanned by the structure sheaf restricted to the component.
\end{lem}
\begin{proof}
    \(\perf(Z^1(W_E,\widehat{T})/\!\!/\widehat{T})\) has the same description, but \(\perf([Z^1(W_E,\widehat{T})/\widehat{T}])_0\) is just pulled back from this (and it is closed under finite colimits and retracts and the decomposition into connected components is compatible).
\end{proof}
\begin{rem}\label{rem: whittaker sheaf for tori}
	In general the functor in the categorical form of Fargues's conjecture depends on a choice of Whittaker datum, determining the image of the structure sheaf of $[Z^1(W_E,\widehat{T})/\widehat{T}]$.
	For $T$ the unipotent radical is trivial, so there is a unique choice of Whittaker datum, giving us the representation $\cind{*}{T(E)}1$
	On the other hand, the $T(E)$-representation corresponding to the structure sheaf on $[Z^1(W_E,\widehat{T})/\widehat{T}]$ under the equivalence \(\qcoh([Z^1(W_E,\widehat{T})/\widehat{T}])_0\simeq\qcoh(\Hom(T(E),\gm))\simeq T(E)\mhyphen\Mod_{\zl}\) is given by the colimit $\varinjlim\zl[T(E)/U]$ over some neighborhood basis of the identity \(U\)
	\begin{align*}
        \zl[T(E)/U]&=\zl[T(E)/U']_{U}\\
        &\cong\zl[T(E)/U']^{U/U'}\\
        &\subset\zl[T(E)/U']
    \end{align*}
    whenever \(U'\subset U\).
	Each $\zl[T(E)/U]$ can be considered as functions $T(E)\to\zl$ with support being the union of finitely many translates of $T(E)/U$.
	We therefore get a map
	\[
		\zl[T(E)/U]\to\cind{*}{T(E)}1
	\]
	This is compatible with the transition maps and since $\bigcap U=*$ it is an isomorphism.
    Using \cref{cor: subcategory given by e_P} we can compute the coinvariants as \((\cind{*}{T(E)}1)_{U}=\zl[T(E)/U]\) when \(U\subset T(E)\) is an open pro-\(p\) subgroup.
\end{rem}